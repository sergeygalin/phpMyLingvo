<?php
  // This software is distributed under GNU General Public License, ver. 2
  // or higher (at your option), released by Free Software Foundation. You can
  // find text of GNU GPL at
  //   http://sageshome.net/GPL.php
  // or
  //   http://www.gnu.org/
  //
  // Copyright(C) Sergey A. Galin, 2003-2004.

/*
  ptkmydump.php Comment:

  Dumping script for ptkdic dictionaries
  from Mysql to "ptk_dictionary_name" files.

  As is, no warranties. Use at your own risk.
  Licence: Use at your discretion.

  Author: Alexander Goriachev <thorn_st@bk.ru>
*/

  require_once("DB.php");
  require_once("config.php");
  require_once("formvars.php");
  require_once("functions.php");
  CheckAdminLogged();
  OldFormVars("dictionary", "file");

  echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>PtkDic Dictionary Dump Maker</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="Author" content="Sergey A. Galin, http://sageshome.net" />
  <link rel="StyleSheet" type="text/css" href="style.css" />
</head>
<body class="admin">
<h1>PtkDic Dictionary Dump Maker</h1>
<div class="pad10">
<?php

  $dictionary=Get("dictionary");
  $file=Get("file");

  if(!strlen($dictionary) || !strlen($file))
    die("Not enough parameters!");
  if(strstr($file, "/")!==false || strstr($file, "\\")!==false ||
    substr($file, strlen($file)-4)!==".bz2" || $file{0}==".")
    die("The file name must not contain slashes or start from period; ".
      "extension must be &quot;.bz2&quot;.");

  echo "<p>Dumping dictionary $dictionary to ",htmlspecialchars($file),"...</p>\n";

  if(file_exists("dumps/$file")){
    echo "<p>File already exists, overwriting...</p>\n";
    unlink("dumps/$file");
  }

  ini_alter("magic_quotes_sybase", 1); // \' -> ''

  if(!isset($bzip2_executable)) $bzip2_executable="bzip2";

  echo "<p>Trying to pipe through external bzip2 program...</p>\n";
  $pipe="$bzip2_executable -c9 > dumps/$file";
  $outfile=popen($pipe, 'w');
  // We try to write one character to check if the pipe is actually works...
  if(!$outfile || !fputs($outfile, "\n")){
    echo "<p>Could not create output pipe!</p>\n";
    if($outfile) pclose($outfile);
    if(!extension_loaded('bz2'))
      die("<p><strong>bzip2 is not available, please install either bzip2 executable or PHP module!</strong></p>\n");
    else{
      // Try to pipe through bz2 extension
      echo "<p><strong>Retry via built-in bzip2...</strong></p>\n";
      $outfile=bzopen("dumps/$file", "w");
      if(!$outfile){
        die("<p><strong>bzip2 is available as a PHP module, but opening file dumps/$file for writing failed. This maybe either the file/directory access permissions problem or the buggy module (try to install bzip2 program).</strong></p>\n");
      }else{
        echo "<p>bzip2 write stream opened.</p>\n";
        $writefn="bzwrite";
        $closefn="bzclose";
      };
    };
  }else{ // opened pipe successfully
    echo "<p>Pipe opened.</p>\n";
    $writefn="fputs";
    $closefn="pclose";
  }


  $writefn($outfile,
    "-- SQL Dump of PtkDic dictionary $dictionary.\n".
    "-- Dumped by phpMyLingvo Dictionay Dump Tool\n".
    "-- Written by Sergey A. Galin.\n".
    "-- Based on ptkmydump.php by Alexander Goriachev AKA Thorn.\n".
    "\n")
      or die("Error: Cannot write header to the output file.");

  //$PgsqlTable = "CREATE TABLE $dictionary (\n".
  //"    art_id integer DEFAULT '0' NOT NULL,\n".
  //"    word character varying(255) DEFAULT '' NOT NULL,\n".
  //"    art_txt longtext NOT NULL\n".
  //");\n\n";

  $createqry=
    "CREATE TABLE $dictionary (\n".
    "  art_id    INT UNSIGNED NOT NULL DEFAULT 0,\n".
    "  word      VARCHAR(255) NOT NULL,\n".
    "  art_txt   LONGTEXT NOT NULL,\n".
    "  UNIQUE art_id_idx (art_id),\n".
    "  INDEX word_idx (word)\n".
    ") ". ((!$process_charset)?"DEFAULT CHARSET=utf8":""). ";\n\n";

  $writefn($outfile, $createqry) or
    die("Error: Cannot write table to the output file: $dictionary");

  $res1=$db->query("SELECT * FROM $dictionary ORDER BY art_id");
  if (DB::isError($res1))
    die ("Unable to get data from table: $dictionary: ".$res1->getMessage());

  $count=0;
  flush();
  while ($row = $res1->fetchRow())
  {
    if (DB::isError($row)) die ("Unable to get row from table: $dictionary");

    $art_id = $row[art_id];
    $word = UniversalQuote($row[word]);
    $art_txt = UniversalQuote($row[art_txt]);

    $str = "INSERT INTO $dictionary VALUES ($art_id, '$word', '$art_txt');\n";
    $writefn($outfile, $str) or die("Error: Cannot write data to the output file.");


    set_time_limit(180);
    if(!($count++ % 1000)){ echo ". "; flush(); }

  }

  // Need to add a few empty lines to avoid unpacking problems under Win
  $writefn($outfile, "\n\n\n");

  //$writefn($outfile, "\n".
  //  "CREATE INDEX ".$dictionary."_ndx1 ON $dictionary (art_id);\n".
  //  "CREATE INDEX ".$dictionary."_ndx2 ON $dictionary (word);\n".
  //  "\n")
  //  or die("Error: Cannot write to the output file.");

  $closefn($outfile);

  echo "<p>Dumping complete, $count record(s).</p>\n";

  $res1->free();

  @chmod("dumps/$file", 0666);

  $db->disconnect();

  echo "<p><strong><a href=\"admin_dictinstall.php\">Click Here To Continue</a></strong></p>\n";

?>
</div>
</body>
</html>
