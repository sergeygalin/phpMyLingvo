
This directtory contains stripped versions of PEAR and PEAR::DB classes for 
installation of phpMyLingvo to a server where PEAR cannot be installed/enabled
system-wide, e.g. some badly supported Windows-based hosting.

Just unpack the archive / copy files and directories to the directory where
you installed your copy of phpMyLingvo and it should work.

