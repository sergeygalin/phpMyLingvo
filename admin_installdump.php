<?php
  // This software is distributed under GNU General Public License, ver. 2
  // or higher (at your option), released by Free Software Foundation. You can
  // find text of GNU GPL at
  //   http://sageshome.net/GPL.php
  // or
  //   http://www.gnu.org/
  //
  // Copyright(C) Sergey A. Galin, 2003-2004.

  require_once("DB.php");
  require_once("config.php");
  require_once("functions.php");
  require_once("formvars.php");
  CheckAdminLogged();
  OldFormVars("file");

  $try_bzip2pipe = true;

  //$nosql=true;
  $nosql=false;
  $db_warning="[No DB warning]";

  // This function writes into $beforeimport, $afterimport (arrays) SQL
  // queries which must be executed for the current $TypeSQL.
  function GetSQLCode($tablename, &$beforeimport, &$afterimport){
    global $TypeSQL, $Database, $db_warning, $process_charset;
    switch($TypeSQL){
      case "pgsql": // PostgreSQL
        $beforeimport=array(
            "CREATE TABLE $tablename (\n".
            "  art_id integer DEFAULT '0' NOT NULL,\n".
            "  word character varying(255) DEFAULT '' NOT NULL,\n".
            "  art_txt text NOT NULL\n".
            ");"
          );
        $afterimport=array(
            "CREATE UNIQUE INDEX ".$tablename."_art_id_idx ON $tablename (art_id);",
            "CREATE INDEX ".$tablename."_word_idx ON $tablename (word);"
          );
        $msg="Using PostgeSQL queries...";
        $db_warning="This is a good DB, I do not expect any problems...";
        break;

      case "mysql": // MySQL
        $beforeimport=array(
            "CREATE TABLE $tablename (\n".
            "  art_id INT UNSIGNED NOT NULL,\n".
            "  word VARCHAR(255) NOT NULL,\n".
            "  art_txt LONGTEXT NOT NULL,\n".
            "  UNIQUE ".$tablename."_art_id_idx (art_id),\n".
            "  INDEX ".$tablename."_word_idx (word)\n".
            ") ". ((!$process_charset)?"DEFAULT CHARSET=utf8":""). ";\n\n"
          );
        $afterimport=array();
        $msg="Using MySQL queries...";
        $db_warning="MySQL is the best of all SQL servers!";
        break;

      case "mssql": // MS-SQL
        $beforeimport=array(
            "CREATE TABLE $tablename (\n".
            "  art_id INT NOT NULL,\n".
            "  word VARCHAR(255) NOT NULL,\n".
            "  art_txt TEXT NOT NULL,\n".
            ");"
          );
        $afterimport=array(
            "CREATE UNIQUE INDEX ".$tablename."_art_id_idx ON $tablename (art_id);",
            "CREATE INDEX ".$tablename."_word_idx ON $tablename (word);"
          );
        $msg="Using Micro\$oft SQL queries...";
        $db_warning="M\$ SQL is not very good and not well tested. Also it's pretty slow, so sit back and relax!";
        break;


      case "sqlite": // Unknown SQL server
        $beforeimport=array(
            "BEGIN TRANSACTION;",
            "CREATE TABLE $tablename (\n".
            "  art_id integer NOT NULL,\n".
            "  word character varying(255) DEFAULT '' NOT NULL,\n".
            "  art_txt text NOT NULL\n".
            ");",
            "CREATE UNIQUE INDEX ".$tablename."_art_id_idx ON $tablename (art_id);",
            "CREATE INDEX ".$tablename."_word_idx ON $tablename (word);"
          );
        $afterimport=array(
            "COMMIT TRANSACTION;"
          );
        $msg="Using SQLite queries...";
        $db_warning="This DB should work OK, but without fulltext search!";
        break;


      default: // Unknown SQL server
        $beforeimport=array(
            "CREATE TABLE $tablename (\n".
            "  art_id integer NOT NULL,\n".
            "  word character varying(255) DEFAULT '' NOT NULL,\n".
            "  art_txt text NOT NULL\n".
            ");"
          );
        $afterimport=array(
            "CREATE UNIQUE INDEX ".$tablename."_art_id_idx ON $tablename (art_id);",
            "CREATE INDEX ".$tablename."_word_idx ON $tablename (word);"
          );
        $msg="Warning: your SQL server type ($TypeSQL) is not ".
          "officially supported by phpMyLingvo yet. Using default queries...";
        $db_warning="We have not tested this DB yet. Please share your experience with us!";
        break;
    }
    return $msg;
  }

  function RunQuery($qry){
    global $nosql, $db;
    echo "<pre class=\"showquery\"><small>",
      htmlspecialchars($qry),"</small></pre>\n";
    if(!$nosql){
      $res=$db->query($qry);
      if(DB::isError($res)){
        echo "<div style='color: red'>Error: ", $res->getMessage(), "</div>\n";
        die("Stopping...");
      }else
        echo "<div style='color: blue'>Success.</div>\n";
    }else
      echo "<div style='color: blue'>(query not executed)</div>\n";
    echo "<div class=\"divider\"></div>\n";
  }

  function RunQueries($comment, &$arr){
    echo "<p>$comment</p>\n";
    foreach($arr as $qry) RunQuery($qry);
  }

  $dictionary_created=false;
  $table_name=false;
  $queries_before=array();
  $queries_after=array();

  function UseLine($line){
    global $TypeSQL, $db, $dictionary_created, $table_name, $queries_before,
      $queries_after, $query_count, $nosql, $db_warning;
    $str=explode(" ", $line, 4);
    $first=strtoupper(trim($str[0]));
    if($first!="INSERT") return;
    // OK now we are sure that the line is "INSERT" query...

    if(!$dictionary_created){
      if(strtoupper(trim($str[1]))=="INTO") $idx=2; else $idx=1;
      $table_name=trim($str[$idx]);
      echo "<p>Table name detected: $table_name</p>\n";
      // Generate table creation queries for this $table_name
      $msg=GetSQLCode($table_name, $queries_before, $queries_after);
      echo "<p>$msg</p>\n";
      echo "<p><big><strong>$db_warning</strong></big></p>\n";
      RunQueries("Running initial queries...", $queries_before);
      $dictionary_created=true;
      echo "<p>Feeding data into the table...</p>\n";
    };
    //RunQuery($line);
    //echo htmlspecialchars($line), "<br>\n";
    if(!$nosql){
      $res=$db->query(trim($line));
      if(DB::isError($res))
        die("Query failed:\n<pre class=\"showquery\">".
          htmlspecialchars($line)."</pre>\n".$res->getMessage()."\n");
    }
  }

  echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>PtkDic Dictionary Dump Installer</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="Author" content="Sergey A. Galin, http://sageshome.net" />
  <link rel="StyleSheet" type="text/css" href="style.css" />
</head>
<body class="admin">
<h1>PtkDic Dictionary Dump Installer</h1>
<div class="pad10">

<?php

  $file=Get("file");
  if(!strlen($file))
    die("Not enough parameters!");
  if(strstr($file, "/")!==false || strstr($file, "\\")!==false ||
    substr($file, strlen($file)-4)!==".bz2" || $file{0}==".")
    die("<br>\nThe file name must not contain slashes or start from period; ".
      "extension must be &quot;.bz2&quot;.\n");
  if(!file_exists("dumps/$file"))
    die("<br>\nFile not exists: ".htmlspecialchars("dumps/$file")."\n");

  echo "<p>Installing ", htmlspecialchars($file), " into $Database DB.</p>\n";



  if(!isset($bzip2_executable)) $bzip2_executable="/bin/bzip2";

  if ($try_bzip2pipe)
  {
    echo "<p>Trying to pipe from bzip2 command...</p>\n";
    $pipecmd="$bzip2_executable -dc dumps/$file";
    $infile=popen($pipecmd, 'r');
    // We try to read a line from the pipe to check if it actually works
    // (it doesn't contain anything useful to us, because we skip at least
    // one query - CREATE TABLE).
  }
  if($try_bzip2pipe && $infile && strlen(fgets($infile))){
    echo "<p>Pipe opened.</p>\n";
    $readfn="fgets";
    $readtype="lines";
    $count=0;
    while(!feof($infile)){
      if(!(++$count % 1000)){
        echo ". ";
        flush();
      }
      set_time_limit(180);
      UseLine(fgets($infile, 1024*1024));
    }
    pclose($infile);
  }else{
    echo "<p><strong>Could not create input pipe through bzip2.</strong></p>\n";
    if($infile) pclose($infile);
    // Pipe not opened, but there is a chance that we have bz2 module.
    if(extension_loaded('bz2')){
      echo "<p><strong>Retry via built-in bzip2...</strong></p>\n";
      flush();
      $infile=bzopen("dumps/$file", "r") or
        die("<p><strong>bzip2 is available as a PHP module, but opening file dumps/$file for reading failed. Please check the file/directory access permissions.</strong></p>\n");
      echo "<p>bzip2 read stream opened.</p>\n";
      $readfn="bzread";
      $closefn="bzclose";
      $tail="";
      //$bytecount=0;
      $abortit=false;
      $readsize=1024*64;
      do{
        set_time_limit(180);
        $buffer=bzread($infile, $readsize);
        $readlen=strlen($buffer);
        //echo "[", nl2br(htmlspecialchars($buffer)), "] ";

        //$bytecount+=strlen($buffer); echo "[$bytecount] ";
        if(!strlen($buffer)) break;
        $buffer=$tail.$buffer;
        while(($pos=strpos($buffer, "\n"))!==false){
          $line=trim(substr($buffer, 0, $pos));
          // !!!!!!!!!!!!!!!!!! WORKAROUND !!!!!!!!!!!!!!!!!!
          // On Window$ the installer can hang without this check.
          if($query_count>1 && !strlen($line)){ $abortit=true; break; };
          UseLine($line);
          $buffer=substr($buffer, $pos+1);
        }
        $tail=$buffer;
        echo ". ";
        flush();
      }while(!feof($infile) && !$abortit /* && $readlen==$readsize*/);
      echo "<p>Ended reading the stream: feof=", feof($infile), ", aborted=", $abortit, ", readlen=$readlen, readsize=$readsize</p>";
      if(strlen($tail)) UseLine($tail);
      bzclose($infile);
    }else{ // no extension
      die("<p><strong>bzip2 is not available, please install either bzip2 executable or PHP module!</strong></p>\n");
    }
  }



  // Finalizing installation
  RunQueries("Running final queries...", $queries_after);

  echo "<p>Installation finished.</p>\n";

  echo "<p><strong><a href=\"admin_dictinstall.php\">Click Here To Continue</a></strong></p>\n";

?>

</div>
</body>
</html>
