<?php
  // This software is distributed under GNU General Public License, ver. 2
  // or higher (at your option), released by Free Software Foundation. You can
  // find text of GNU GPL at
  //   http://sageshome.net/GPL.php
  // or
  //   http://www.gnu.org/
  //
  // Copyright(C) Sergey A. Galin, 2003-2004.
  //
  // MS-SQL support idea by Alexander Goryachev AKA Thorn.
  //
  // This is a database abstraction layer for phpMyLingvo, based on
  // standard PEAR::DB class. It uses mostly the same interface functions
  // as PEAR::DB but adds additional DB and OS compatibility functionality.
  //

  $db_loaded=include_once("DB.php"); // PEAR module
  if(!$db_loaded)
    die("\n<br>\n<br>\n<strong>PEAR::DB module cannot be loaded. ".
      "Please insure that PEAR is installed ".
      "and can be found by include path specified in your php.ini file, or ".
      "download and install a stripped version of PEAR and DB from ".
      "phpMyLingvo Download page.</strong><br>\n");

  require_once("charset_functions.php");


  function ptk2mssql(&$str){
    // In dumps we use LF character encoded as \n, but MS-SQL wants it "as is".
    // We also convert charset to CP866 so MS tools could see Russian characters
    // in the database.
    return str_replace('\n', "\n", convert_cyr_string($str, "k", "a"));
  }

  function mssql2ptk(&$str){
    // MS wants CP866 for Russian, we use KOI8-R here.
    return convert_cyr_string($str, "a", "k");
  }

  function ptk2sqlite(&$str){
    return str_replace('\n', "\n", $str);
  }



  // This class wraps successfull DB query result.
  // We don't use "extends" because we don't know which type of
  // object we are going to "extend".
  class DictResult{
    var $pearresult, $fetchRecode;

    function DictResult($pr, $fetchRecoder){
      $this->pearresult=$pr;
      $this->fetchRecode=$fetchRecoder;
    }

    function fetchRow(){
      $result=$this->pearresult->fetchRow();
      if(sizeof($result) && $this->fetchRecode!==false){
        $function=$this->fetchRecode;
        foreach($result as $key=>$value)
          $result[$key]=$function($value);
      }
      return $result;
    }

    function free(){
      $this->pearresult->free();
    }

    function getMessage(){
      $this->pearresult->getMessage();
    }

  };


  // This class wraps database class returned by DB::connect.
  // We don't use "extends" because we don't know which type of
  // object we are going to "extend".
  class DictDB{
    var
      $peardb, // Sub-object to perform link to database.
      $fetchRecode, // Names of charset recoding functions to use on
      $queryRecode, // queries and results; set to false if no recoding needed.
      $sqlDriver; // SQL driver name string (mysql, pgsql, and etc.)

    function DictDB($TypeSQL, $Host, $User, $Password, $Database,
      $Persistent, $Debug){

      $this->fetchRecode=false;
      $this->queryRecode=false;
      $this->sqlDriver=$TypeSQL;

      $options=array(
        'persistent' => $Persistent,
        'debug'      => $Debug
      );
      $this->peardb=DB::connect("$TypeSQL://$User:$Password@$Host/$Database",
        $options);
      if(DB::isError($this->peardb))
        die ("DictDB::DictDB error: PEAR::DB SQL Connection Failed: ".
          $this->peardb->getMessage());
      $this->peardb->setFetchMode(DB_FETCHMODE_ASSOC);

      // Setting up database-specific options
      switch($TypeSQL){
        case "mssql":
          $this->fetchRecode="mssql2ptk";
          $this->queryRecode="ptk2mssql";
	  break;
	case "sqlite":
	  $this->queryRecode="ptk2sqlite";
	  break;  
      };

    }

    // On error, we return usual PEAR::DB object. On success, we return
    // the object wrapped into our class to enable our compatibility
    // functions.
    function query($qry){
      if($this->queryRecode!==false){
        $function=$this->queryRecode;
        $qry=$function($qry);
      };
      $result=$this->peardb->query($qry);
      if(DB::isError($result))
        return $result;
      else
        return new DictResult($result, $this->fetchRecode);
    }

    function getListOf($what){
      return $this->peardb->getListOf($what);
    }

    // This function also adds quotes around strings (as its predecessor)
    function quote($str){
      return $this->peardb->quote($str);
    }

    function disconnect(){
      return $this->peardb->disconnect();
    }

    // Create SQL code which returns TRUE if $needle is a substring of
    // $haystack. The arguments must be valid SQL strings or field names,
    // i.e. string values must be pre-quoted via DictDB::quote().
    function IsSubstringFunction($needle, $haystack){
      switch($this->sqlDriver){
        case "mssql": return "CHARINDEX($needle, $haystack)>0";

        case "sqlite": return "SUBSTR($haystack, 1, LENGTH($needle))=$needle"; 
	
        default: return "POSITION($needle IN $haystack)>0";
      };
    }
    
    function CanSearchSubstring(){
      switch($this->sqlDriver){      
        case "sqlite": return false;
	default: return true;    
      }
    }


  };


?>