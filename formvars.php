<?php
  // Copyright(C) Sergey A. Galin, 2003-2004.

  // This function will insure that $_GET, $_POST will be filled
  // even on old PHP version with register_globals = Off.
  // USAGE: OldFormVars("var1", "var2"....)
  // With empty parameters, values will be recvered only from
  // HTTP_XXX_VARS.
  function OldFormVars(){
    global $_GET, $_POST, $HTTP_GET_VARS, $HTTP_POST_VARS;
    if(sizeof($_GET) || sizeof($_POST)) return;
    if(sizeof($HTTP_GET_VARS) || sizeof($HTTP_POST_VARS)){
      $_GET=$HTTP_GET_VARS;
      $_POST=$HTTP_POST_VARS;
      return;
    }
    $_GET=array();
    $_POST=array();
    $numargs=func_num_args();
    for($i=0; $i<$numargs; $i++){
      $arg=func_get_arg($i);
      global ${$arg};
      $_GET[$arg]=${$arg};
      $_POST[$arg]=${$arg};
    }
  }

  function OldFormVarsA($varnames){
    global $_GET, $_POST, $HTTP_GET_VARS, $HTTP_POST_VARS;
    if(sizeof($_GET) || sizeof($_POST)) return;
    if(sizeof($HTTP_GET_VARS) || sizeof($HTTP_POST_VARS)){
      $_GET=$HTTP_GET_VARS;
      $_POST=$HTTP_POST_VARS;
      return;
    }
    $_GET=array();
    $_POST=array();
    foreach($varnames as $arg){
      global ${$arg};
      $_GET[$arg]=${$arg};
      $_POST[$arg]=${$arg};
    }
  }

  // This function will fill $_SERVER variable even on old PHP.
  // USAGE: OldGlobals();
  function OldGlobals(){
    global $_SERVER, $HTTP_SERVER_VARS;
    if(sizeof($_SERVER)) return;
    if(sizeof($HTTP_SERVER_VARS)){
      $_SERVER=$HTTP_SERVER_VARS;
      return;
    }
    $vars=array(
      "UNIQUE_ID", "HTTP_USER_AGENT", "HTTP_ACCEPT",
      "HTTP_ACCEPT_LANGUAGE", "HTTP_ACCEPT_ENCODING",
      "HTTP_ACCEPT_CHARSET", "HTTP_KEEP_ALIVE", "HTTP_CONNECTION",
      "HTTP_REFERER", "HTTP_COOKIE", "HTTP_PATH", "SERVER_SIGNATURE",
      "SERVER_SOFTWARE", "SERVER_NAME", "SERVER_ADDR", "SERVER_PORT",
      "REMOTE_ADDR", "DOCUMENT_ROOT", "SERVER_ADMIN", "SCRIPT_FILENAME",
      "REMOTE_PORT", "REMOTE_USER", "AUTH_TYPE", "GATEWAY_INTERFACE",
      "SERVER_PROTOCOL", "REQUEST_METHOD", "QUERY_STRING", "REQUEST_URI",
      "SCRIPT_NAME", "PHP_SELF", "PHP_AUTH_USER", "PHP_AUTH_PW");
    $_SERVER=array();
    foreach($vars as $var){
      global ${$var};
      $_SERVER[$var]=${$var};
    }
  }

  // This function allows to work with $_SESSION array
  // on older PHP configurations.
  // USAGE: OldSessionVars("var1", "var2"...);
  function OldSessionVars(){
    global $_SESSION;
    if(isset($_SESSION)) return;
    if(!sizeof($_SESSION)) $_SESSION=array();
    $numargs=func_num_args();
    for($i=0; $i<$numargs; $i++){
      $arg=func_get_arg($i);
      if(!isset($_SESSION[$arg])){
        global ${$arg};
        $_SESSION[$arg]=${$arg};
      }
    }
    register_shutdown_function("SaveOldSessionVars");
  }

  // You don't have to call this function, it's invoked automatically.
  function SaveOldSessionVars(){
    global $_SESSION;
    foreach($_SESSION as $key=>$value){
      global ${$key};
      ${$key}=$value;
    }
  }

  // Copy cookie values from global variables of old PHP version
  // to new-style $_COOKIES array.
  // USAGE: OldCookies("var1", ...);
  function OldCookies(){
    global $_COOKIE;
    if(!sizeof($_COOKIE)) $_COOKIE=array(); else return;
    $numargs=func_num_args();
    for($i=0; $i<$numargs; $i++){
      $arg=func_get_arg($i);
      if(!isset($_COOKIE[$arg])){
        global ${$arg};
        $_COOKIE[$arg]=${$arg};
      }
    }
  }

  function Request($varname){
    global $_POST, $_GET;
    if(isset($_POST[$varname])){
      return (get_magic_quotes_gpc())?
        stripcslashes($_POST[$varname]):$_POST[$varname];
    }elseif(isset($_GET[$varname])){
      return (get_magic_quotes_gpc())?
        stripcslashes($_GET[$varname]):$_GET[$varname];
    }else return false;
  }

  function Post($varname){
    global $_POST;
    if(isset($_POST[$varname]))
      return (get_magic_quotes_gpc())?
        stripcslashes($_POST[$varname]):$_POST[$varname];
    else return false;
  }

  function Get($varname){
    global $_GET;
    if(isset($_GET[$varname]))
      return (get_magic_quotes_gpc())?
        stripcslashes($_GET[$varname]):$_GET[$varname];
    else return false;
  }
?>
