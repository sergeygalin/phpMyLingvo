<?php
  // This software is distributed under GNU General Public License, ver. 2
  // or higher (at your option), released by Free Software Foundation. You can
  // find text of GNU GPL at
  //   http://sageshome.net/GPL.php
  // or
  //   http://www.gnu.org/
  //
  // Copyright(C) Sergey A. Galin, 2003.


  // $version must be defined before this file included.
  // charset_functions.php must be included BEFORE this file.

  require_once("abstract_db.php");

  // START OF CONFIGURATION STRINGS

  // Password to access Admin Tool. It may be presented as a cleartext
  // or as md5 hash.
  $admin_password="admin"; // The password cannot be empty!

  // A command which runs bzip2 executable. It's used by dump maker and
  // installer. They also can live without bzip2 executable if your PHP
  // has properly working/enabled bz2 module.
  $bzip2_executable="bzip2";

  // $TypeSQL - Type of your database. Must be one of:
  //   "mysql"  -> MySQL
  //   "pgsql"  -> PostgreSQL
  //   "ibase"  -> InterBase
  //   "msql"   -> Mini SQL
  //   "mssql"  -> Microsoft SQL Server
  //   "oci8"   -> Oracle 7/8/8i
  //   "odbc"   -> ODBC (Open Database Connectivity)
  //   "sybase" -> SyBase
  //   "ifx"    -> Informix
  //   "fbsql"  -> FrontBase
  // Please note that not all above databases are supported
  // by PHP by degfault. Make sure that your PHP has appropriate module
  // and it's enabled in your php.ini file.

  $TypeSQL="mysql";
  $Host="localhost"; // SQL server host
  $User="root"; // SQL server username
  $Password="anyword"; // SQL server password
  $Database="ptkdic"; // SQL database name
  $Persistent=false; //Use persistent connection to SQL server? (true/false)
  $Debug=true; // Print debug messages from SQL server? (true/false)


  // PostgreSQL
  // $TypeSQL="pgsql";$Host="";$User="ptkdic";$Password="ptkdic";$Database="ptkdic";

  // MS-SQL
  //$TypeSQL="mssql"; $Host="localhost"; $User="ptkdic"; $Password="ptkdic";
  
  // SQLite (built-in into PHP5)
  // To create SQLite database, simply create an empty file called e.g. SQLite.db,
  // Make sure it's readable and writable by PHP scripts. 
  // For instance (Linux): $ touch SQLite.db ; chmod a+rw SQLite.db
  // $TypeSQL="sqlite"; $Host=""; $User=""; $Password=""; $Database="SQLite.db";
  

  //Database connect
  $db = new DictDB($TypeSQL, $Host, $User, $Password, $Database, $Persistent, $Debug);


  // Variables for HTML headers.
  // $version is set in index.php before including this file.
  $title="phpMyLingvo v$version @ $_SERVER[SERVER_NAME]";
  $description="$title - a free, universal dictionary/glossary ".
   "front-end to dictionaries stored in MySQL in PtkDic/GtkDic/JaLingvo ".
   "compatible format. ".
   "Web interface by Sergey A. Galin, distributed under GNU GPL.";

  // Cookie expiration date:
  $cookie_expire=time()+3600*24 * 365;

  // *******************************************************************
  // *** Multi-byte support.
  // *******************************************************************

  // Enable Multibyte Strings. This is neccessary for support for all
  // input characters for stupid browser (Opera, Konqueror) which support
  // UNICODE but can't submit it via entities.
  $enable_multibyte=true;

  // Codec to convert UTF-8 to ANSI when $enable_multibyte=true.
  // This can be set to:
  // "sg" (default, RECOMMENDED) - internal codec written in PHP, slower
  // than mbstring but much more reliable (compatible with any
  // version/installation of PHP).
  // "mbstring" - use experimental PHP extension - mbstring
  // "recode" - call external program - GNU Recode. Left for
  // testing and backward compatibility only.
  $multibyte_codec="sg"; //"recode"; //"mbstring";

  // Configure page Charset value accordingly to browser capabilities.
  if ($process_charset) {
    if(!$enable_multibyte ||
      strpos(strtoupper($_SERVER["HTTP_ACCEPT_CHARSET"]), "UTF-8")===false ||
      strpos($HTTP_USER_AGENT, "Links")!==false ||
      strpos($HTTP_USER_AGENT, "Lynx")!==false)
    {
      // => Then browser without (correct) UTF-8 support OR multibyte disabled.
      // Choose ISO-8859-1 ONLY if you don't have any Russian dictionaries.
      $charset="KOI8-R"; // "ISO-8859-1";
      mysql_set_charset("koi8-r");
    } else {
      $charset="UTF-8";
      if ($TypeSQL==="mysql") {
        mysql_set_charset("utf8");
      }
      mb_internal_encoding('UTF-8');
    }
  } else {
    if ($TypeSQL==="mysql") {
       mysql_set_charset("utf8");
    }
    mb_internal_encoding('UTF-8');
    $charset="UTF-8";
  }

  // Set UTF-8 usage flag. Default expression ($charset=="UTF-8") means
  // that UTF-8 output will be used when it is set as Charset.
  $utf8=($charset=="UTF-8");
  //$utf8=false;

  $fix_koi_search_problem = $process_charset;

  // UNICODE support:
  // It is highly recommended to have all options ON (=true).
  // You can turn them off for testing or if you want to support very-very
  // old browsers (at cost of big problems with newer software).
  // Output in &#... UNICODE or UTF-8 (accordingly to $utf8):
  $process_charset=false; // Set to true or to $utf8 !

  // Input in &#... UNICODE. Has no effect when $utf8==true.
  $input_unicode=true;

  // Kill leading and trailing spacing from search string.
  $trim_input=true;

  // System-related settings:
  // Path to recode. Needed only in $enable_multibyte mode and
  // with $multibyte_codec="recode".
  //$recode_path="/usr/bin/recode";
  // Set this to true if your SERVER runs under Windows and you
  // are using $multibyte_codec="recode".
  //$windows_mode=false;

  // Server load limitations:
  //
  // Limit number of dictionaries to search at once
  $max_dictionaries=100;
  // Allow/disallow full-text searching.
  $allow_fulltext=$db->CanSearchSubstring();
  // Limit number of dictionaries to search at once in full-text mode
  $max_dictionaries_fulltext=100;
  // Max number of search results allowed
  $max_search_results=100;
  // Minimal length of query allowed for substring and full-text search.
  $min_substring=3; // 3 recommended!
  // Set to 1 to have all dictionaries selected by default (when no
  // cookies set); to 0 to have no selected dictionaries by default.
  $default_dictionary_selection=0;

  // Enable gzip conversion of output HTML (when browser supports it).
  // Very small additional load to server's CPU but decrease of traffic
  // (especially with a lot of UTF output). Compression can be about as
  // high as 6X with lots of Cyrillic characters in UNICODE.
  // Highly recommended to enable this option on large and/or slow network.
  $allow_compression=true;

  // Name of file to be included into the page when no query entered.
  $welcome_text="welcome.inc.html";
  // Name of file to be included at bottom of the page (usually
  // contains banners and counters).
  //$bottom_code="../../include/spylogr.html";
  $bottom_code="";

  // Allow Case-Insensitive searches.
  $show_case_insensitive=true;

  // HTTP path for phpMyLingvo cookie (if not sure, leave "/")
  $cookie_path="/";

  // If you want PHP Dic to detect list of available dictionaries
  // automatically, set $diclist as empty array here.
  $diclist=array();
  // If you want PHP Dic to use pre-defined set of dictionaries,
  // declare it here like this (see below). That would allow to use
  // PHP Dic e.g. when PtkDic's dictionaries are loaded into non-PtkDic
  // databse (along with tables in other formats).
  /*
  $diclist=array(
    array("Name" => "eng_rus_slang",
          "Desc" => "English-Russian Slang Dictionary",
          "From" => "English",
          "To"   => "Russian"),
    array("Name" => "engruscomp",
          "Desc" => "English-Russian Computer Dictionary",
          "From" => "English",
          "To"   => "Russian");
  );
  */

  // Dictionary tuning options. No need to remove entries if
  // you don't have some dictionary installed.
  $dict_options=array(
    "english"      => array("disable_transcription"=>true),
    "english2"     => array("disable_transcription"=>true),
    "english3"     => array("disable_transcription"=>true),
    "eng_eng_main" => array("disable_transcription"=>true),
    "eng_eng_nw"   => array("disable_transcription"=>true),
    "eng_eng_syn"  => array("disable_transcription"=>true)
  );



?>
