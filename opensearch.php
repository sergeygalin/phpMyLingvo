<?php
  $version_file=fopen("version.txt", "r");
  $version=trim(fgets($version_file, 100));
  fclose($version_file);

  require_once("config.php");
  header("Content-Type: text/xml; charset=utf-8");
  echo '<?xml version="1.0"?>', "\n";
  $webdir = 'http://'.$_SERVER['SERVER_NAME'].dirname($_SERVER['REQUEST_URI']);
  $qry = $webdir.'/?default_select_all=1&utf8query=1&query=';
?>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/" 
  xmlns:moz="http://www.mozilla.org/2006/browser/search/">
  <ShortName><?php echo $title; ?></ShortName>
  <Description><?php echo $title; ?></Description>
  <Image height="16" width="16" type="image/x-icon"><?php echo htmlspecialchars($webdir); ?>/favicon.ico</Image>
  <Url type="text/html" method="get"
    template="<?php echo htmlspecialchars($qry); ?>{searchTerms}" />
<?php /*
  <Url type="application/x-suggestions+json" method="get"
    template="<?php echo htmlspecialchars($qry); ?>{searchTerms}" />
  <Url type="application/x-suggestions+xml" method="get"
    template="<?php echo htmlspecialchars($qry); ?>{searchTerms}" />
*/ ?>
  <moz:SearchForm><?php echo htmlspecialchars($qry); ?></moz:SearchForm>
</OpenSearchDescription>
