<?php
  // This software is distributed under GNU General Public License, ver. 2
  // or higher (at your option), released by Free Software Foundation. You can
  // find text of GNU GPL at
  //   http://sageshome.net/GPL.php
  // or
  //   http://www.gnu.org/
  //
  // Copyright(C) Sergey A. Galin, 2003-2004.

  // phpMyLingvo function library.
  // config.php must be included before this file!

  require_once("formvars.php");
  session_start();
  // session_register("phpmylingvo_admin");
  OldSessionVars("phpmylingvo_admin");

  $const_listmarkers=array(
    "a", "b", "c", "d",
    "�", "�", "�", "�",
    "V", "VI", "IV", "III", "II", "I"
  );


  // ***************************************
  // Load list of available dictionaries into $diclist[]
  // ***************************************
  function dict_compare($a, $b){ return strcmp($a["Desc"], $b["Desc"]); }
  function LoadDicList($dieifnodicts=true){
    global $diclist, $dict_options, $db, $TypeSQL, $Database;
    if(!sizeof($diclist)){
      $diclist=array();
        $res=$db->getListOf("tables");
            if (DB::isError($res)) die ($res->getMessage());

        for($i=0; $i<count($res); $i++)
        {

        // !!!!!!!!!!!!!!!!!! WORKAROUND !!!!!!!!!!!!!!!!!!!!!!!!
        // Skiping unneeded tables on PostgreSQL and MS-SQL
        if(
            ($TypeSQL=="pgsql" && substr($res[$i], 0, 4)=="sql_.") ||
            ($TypeSQL=="mssql" && $res[$i]=="dtproperties") ||
	    ($TypeSQL=="sqlite" && substr($res[$i], 0, 7)=="sqlite_")
          )
          continue;

        // Get dictionary description
        $res2=$db->query("SELECT * FROM $res[$i] WHERE art_id=3");
        if (DB::isError($res2)) die ("Unable to get info on dictionary $res[$i]: ".$res2->getMessage());
        $row2=$res2->fetchRow();
        if (DB::isError($row2)){  
	  echo "<p><strong>WARNING: Unable to get info on dictionary $res[$i] - row 3 not found!</strong></p>\n";
          array_push($diclist, array("Name"=>$res[$i], "Desc"=>"ERROR",
            "From"=>"UNKNOWN", "To"=>"UNKNOWN"));
	}else{
          $res2->free();
          if(strlen($row2["word"])<1)
            echo "<p><strong>WARNING: Dictionary $res[$i] doesn't have a description at row 3.</strong></p>\n";
          $languages=explode("\n", $row2["art_txt"]);
          array_push($diclist, array("Name"=>$res[$i], "Desc"=>$row2["word"],
            "From"=>$languages[1], "To"=>$languages[3]));
        }	
      // echo "$row2[word] From=>$languages[1], To=>$languages[3]<br />";
      }
      if(sizeof($diclist)<1){
        if($dieifnodicts)
          die("No dictionaries installed in $Database DB. Please read
            <a href=\"readme.html\">Readme</a> for help.");
        else
          return false;
      }
      usort($diclist, "dict_compare");
    }
    // Applying dictionary tuning
    foreach($dict_options as $dc => $ops){
      $idx=-1;
      for($i=0; $i<sizeof($diclist); $i++)
        if($diclist[$i]["Name"]==$dc){ $idx=$i; break; }
      if($idx>=0)
        foreach($ops as $o_name => $o_val)
          $diclist[$idx][$o_name]=$o_val;
//          echo "$dc - ", $diclist[$idx]["Desc"], ": $o_name = $o_val<br />";
    }
    return true;
  }

  /*function repairEntities($s)
  {
    $out = "";
    for ($i = 0; $i < strlen($s); ++$i)
    {
      if ($i < strlen($s)-1 && $s{$i} == '&' && $s{$i+1} == '#')
      {
         $code = "";
         $i += 2;
         for (; $i < strlen($s) && $s{$i} != ';'; ++$i)
           $code .= $s{$i};
         $out .= mb_convert_encoding(chr(intval($code)), "utf-8", "koi8-r");
      }
      else
      {
         $out .= $s{$i};
      }
    }
    return $out;
  }*/


  function PrepareArticle($art, $search, $transcription){
    global $process_charset, $const_listmarkers, $utf8, $charset;
    $cap=htmlspecialchars($art["word"]);

    $text=nl2br(htmlspecialchars($art["art_txt"]));

    // Highligting search query
    $searchv=SGPossibleRegs($search, $charset=='UTF-8');
    if($process_charset){
      $text=ProcessEncoding($text, $transcription);
      $cap=ProcessEncoding($cap, $transcription);
    }
    foreach($searchv as $srch){
      $srch=htmlspecialchars($srch);
      if($process_charset) $srch=ProcessEncoding($srch, $transcription);
      $text=str_replace($srch, "<strong class=\"art_word\">$srch</strong>", $text);
    }
    for($i=30; $i>=1; $i--){
      $text=str_replace("\n$i&gt;", "\n<em class=\"list\">$i&gt;</em>", $text);
      $text=str_replace(" $i&gt;", " <em class=\"list\">$i&gt;</em>", $text);
      $text=str_replace("\n$i)", "\n<em class=\"list\">$i)</em>", $text);
      $text=str_replace(" $i)", " <em class=\"list\">$i)</em>", $text);
      $text=str_replace("\n$i.", "\n<em class=\"list\">$i.</em>", $text);
      $text=str_replace(" $i.", " <em class=\"list\">$i.</em>", $text);
      //$text=str_replace(" $i ", " <b>$i</em> ", $text);
    }
    foreach($const_listmarkers as $mark){
      if($process_charset) $mark=ProcessEncoding($mark, $transcription);
      $text=str_replace("\n$mark)", "<em class=\"list\">$mark)</em>", $text);
      $text=str_replace(" $mark)", " <em class=\"list\">$mark)</em>", $text);
      $text=str_replace("\n$mark. ", "<em class=\"list\">$mark. </em>", $text);
      $text=str_replace(" $mark. ", " <em class=\"list\">$mark. </em>", $text);
    }
    $text=str_replace("Syn:", "<i class=\"hl1\">Syn:</i>", $text);
    $text=str_replace("Syn :", "<i class=\"hl1\">Syn:</i>", $text);
    $text=str_replace("~", "<strong class=\"wildcard\">~</strong>", $text);
    $text=str_replace("*", "<strong class=\"wildcard\">*</strong>", $text);

    // !!!!!!!!!!! Transcription !!!!!!!!!1
    $charcounts=count_chars($text, 0);
    $dif=$charcounts[ord('[')]-$charcounts[ord(']')];
    if($dif==0){
      $text=str_replace("[", "<samp class=\"transcr\">[", $text);
      $text=str_replace("]", "]</samp>", $text);
    }

    //$text=str_replace("(", "<i>(", $text);
    //$text=str_replace(")", ")</i>", $text);

    $text="<p class=\"art_art\"><big class=\"art_title\">$cap</big> ".
           "$text ".
           "<small class=\"art_id\">(#$art[art_id])</small></p>\n";
    return $text;
  }


  function CheckAdminLogged(){
    global $_SESSION;
    if(!$_SESSION["phpmylingvo_admin"])
      die("You must login to Admin section to access this page!\n");
  }


  function Redirect($url){
    //header("Location: $url");
    header("Refresh: 1; url=$url");
    $hurl=htmlspecialchars($url);
    echo "<html>\n<head><title>Redirecting...</title></head>\n",
         "<body><h1>Redirecting...</h1>\n",
         "Click on the link below if your browser doesn't ",
         "redirect automatically:<br />\n",
         "<a href=\"$hurl\">$hurl</a>\n</body>\n</html>\n";
    exit;
  }


  function UniversalQuote($str){
    return str_replace("'", "''", addcslashes($str, "\\\n\r"));
  }



?>
