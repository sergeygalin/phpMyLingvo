#!/usr/bin/php -q
<?php
  // Input: put some unpacked dumps into directory "dumps"
  // Output: create empty dir "out"
  // Run the script
  // ...
  // PROFIT

  // COPY THE FILE NEARBY
  require_once("charset_functions.php");

  $dict_options=array(
    "english"      => array("disable_transcription"=>true),
    "english2"     => array("disable_transcription"=>true),
    "english3"     => array("disable_transcription"=>true),
    "eng_eng_main" => array("disable_transcription"=>true),
    "eng_eng_nw"   => array("disable_transcription"=>true),
    "eng_eng_syn"  => array("disable_transcription"=>true)
  );

  function repairEntities($s)
  {
    $out = "";
    for ($i = 0; $i < strlen($s); ++$i)
    {
      if ($i < strlen($s)-1 && $s{$i} == '&' && $s{$i+1} == '#')
      {
         $code = "";
         $i += 2; // Skip &#
         for (; $i < strlen($s) && $s{$i} != ';'; ++$i)
           $code .= $s{$i};
         $out .= mb_convert_encoding(chr(intval($code)), "utf-8", "koi8-r");
      }
      else
      {
         $out .= $s{$i};
      }
    }
    return $out;
  }

  $dir = opendir("dumps");
  while ($fn = readdir($dir))
  {
    if ($fn{0} == '.')
      continue;
    $trans = !(isset($dict_options[$fn]) && $dict_options[$fn]['disable_transcription']);
    echo "$fn [TRANS: ", ($trans)?1:0, "]...\n";

    $f = fopen("dumps/".$fn, "r");
    $fo = fopen("out/".$fn, "w");

    // 263, 8852
    while (!feof($f))
    {
      $r = fgets($f);
      $o = repairEntities(ProcessEncoding($r, $trans));
      fwrite($fo, $o);
    }

    fclose($f);
    fclose($fo);
    echo "...done.\n";
  }
  echo "I did it!\n";
?>