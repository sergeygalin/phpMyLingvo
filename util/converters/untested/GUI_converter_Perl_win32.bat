@rem = '--*-Perl-*--
@rem @echo off
@rem echo 

@echo off
if "%OS%" == "Windows_NT" goto WinNT
perl -x -S "%0" %1 %2 %3 %4 %5 %6 %7 %8 %9
goto endofperl
:WinNT
perl -x -S %0 %*
if NOT "%COMSPEC%" == "%SystemRoot%\system32\cmd.exe" goto endofperl
if %errorlevel% == 9009 echo You do not have Perl in your PATH.
if errorlevel 1 goto script_failed_so_exit_with_non_zero_val 2>nul
goto endofperl
@rem ';
#!/usr/bin/perl 
#line 15
use Win32::GUI;
use strict;

my $NameOfFile;
my $NameNewFile;
my $page = 'utf8';
my $Win = new Win32::GUI::Window (-sizable=>0,
                                  -pos=>[100, 100],
                                  -size=>[330, 235],
                                  -name=>"Window",
                                  -text=>'GUI xdxf converter 0.1b',
                                  -onTerminate=>sub{return -1;0;}
                                  );
my $path_open = $Win->AddRichEdit(-name=>'REd',
                             -pos=>[5, 20],
                             -size=>[290, 20],
                             #-onChange=>'Change',
                             -onLostFocus=>'Change',
                             -onGotFocus=>'Change',
                             );
my $path_save = $Win->AddRichEdit(-name=>'REd_s',
                             -pos=>[5, 55],
                             -size=>[290, 20],
                             );
my $button_cp1251 = $Win->AddRadioButton(-name=>'cp1251',
                                      -pos=>[10,95],
                                      -onClick=>sub{$page = "cp1251";},
                                      -text=>'Windows cp1251',
                                      );
my $button_koi8 = $Win->AddRadioButton(-name=>'koi8-r',
                                      -pos=>[10,115],
                                      -onClick=>sub{$page = "koi8r";},
                                      -text=>'KOI8-R',
                                      );
my $button_utf8 = $Win->AddRadioButton(-name=>'utf8',
                                      -pos=>[10,135],
                                      -onClick=>sub{$page = "utf8";},
                                      -text=>'UTF-8',
                                      );
my $sv_b = $Win->AddButton(-name=>'save',
                           -pos=>[300, 55],
                           -size=>[20, 20],
                           -text=>'...',
                           -onClick=>'LoadFile',
                           );
$Win->AddLabel(-name=>'label2',
               -pos=>[5,40],
               -size=>[290,15],
               -text=>'���� ����������',
               -align =>'center',
               );
$Win->AddGroupbox(-name=>'group',
               -pos=>[5,80],
               -size=>[120,100],
               -text=>'���������',
               -align =>'center',
               );
$Win->AddButton(-name=>'open',
                -pos=>[300, 20],
                -size=>[20, 20],
                -text=>'...',
                -onClick=>'LoadFile',
                );
$Win->AddLabel(-name=>'label',
               -pos=>[5,5],
               -size=>[290,15],
               -text=>'���� � �������',
               -align =>'center',
               );
my $cv_b = $Win->AddButton(-name=>'Convert',
                           -pos=>[270, 80],
                           -size=>[50, 25],
                           -text=>'Convert',
                           -onClick=>'Convert',
                           );

$path_save->Enable(0);

$sv_b->Enable(0);
$Win->Show();
Win32::GUI::Dialog();
sub LoadFile{
	$Win->Text('GUI xdxf converter 0.1b');
    $NameOfFile = &Win32::GUI::GetOpenFileName (-filter =>['XDXF - XML Dictionary Xchange File','*.xdxf'],
                                                -directory =>"c:\\program files",
                                                -title=>'Select XDXF dictionary'
                                                );
    $path_open->Text($NameOfFile);
    my $pt_save = $NameOfFile;
    $pt_save =~s/\w+\.xdxf$//;
    $path_save->Text($pt_save);
    $NameNewFile = $pt_save;
}
sub Change{
    my $item = shift;
    my $txt = $item->Text;
    if($txt eq ''){
        $cv_b->Enable(0);
    }else{
        $cv_b->Enable(1);
    }
}
sub Convert{
use XML::Simple;
use Encode;
my %lang = (ENG=>'English',RUS=>'Russian');
#my $codepage = 'cp1251';
my $ref = XMLin($NameOfFile);
#{
#use utf8;
my $name = encode("cp1251",$ref->{'full_name'});
	$name =~tr/�������������������������������������Ũ��������������������������/abvgdeejziiklmnoprstufxc4wwiiiiuiABVGDEEJZIIKLMNOPRSTUFXC4WWiiiiui/d;
#}

#my $name = encode($page,$ref->{'full_name'});#VAR1-{'lang_to'} VAR1-{'lang_from'}
#if($name ){
#$name =~s/�/y/g;
#}else{
#my	$name = encode("iso-8859-1",$ref->{'full_name'});
#}
my($to,$from)=($ref->{'lang_to'},$ref->{'lang_from'});
$name =~s/\s/_/g;
my $name_file = $NameNewFile.$name.".sql";
open SQL,">$name_file" or die "@!";

print SQL "CREATE TABLE $name\{\n";
print SQL "       art_id    INT UNSIGNED NOT NULL,\n";#
print SQL "       word      VARCHAR(255) NOT NULL,\n";#
print SQL "       art_txt   LONGTEXT NOT NULL,\n";#
print SQL "       UNIQUE    art_id_idx (art_id),\n";
print SQL "       INDEX     word_idx (word)\n";
print SQL "\};\n";
print SQL "INSERT INTO $name VALUES ('3', '$name', '$name\\n$lang{$from}\\nl\\n$lang{$to}\\nr');\n";

my $i = 10;
my $ky = 0;
foreach my $ar (@{$ref->{ar}}) {
        my $str = encode($page,$ar->{'content'});
		$str=~s/\\$//;
		$str=~s/^\\//;
        $str=~s/^\n//;
        $str=~s/\n/\\n/g;
		$str=~s/\'/&#8217;/g;
		$str=~s/\"/&#8221;/g;
		if($ar->{'k'}=~/^HASH/){
            $ky = '';
        }else{
            $ky = encode($page,$ar->{'k'});
			#$ky=~s/\'/&#8217;/g;
			#$ky=~s/\"/&#8221;/g;
        }
        print SQL "INSERT INTO $name VALUES ('$i','$ky','$str');\n";
        ++$i;
}
close SQL;
my $text = $Win->Text;
$Win->Text($text." - convert complete");
}
__END__
:endofperl