#!/usr/bin/php -q
<?php
  // Mueller Dictionary conversion script v 1.0
  // Written by Sergey A. Galin, 04/2003, http://sageshome.net  
  // Converts dictionaries from Cymbalyuk's format to PtkDict MySQL dump

  // Usage: put the script into the directory as dictionary in "Mueller" format;
  // modify lines below to match the dictionary and run the script with command like:
  // $ ./mueller2pktdc.php > dictionary-dump
  // $ bzip2 -9 dictionary-dump
  // Don't forget to chmod 755 the script :)

$tbl="Mueller7Accent";
$f=fopen("Mueller7accentGPL.koi", "rt");
$h="English-Russian Mueller Dictionary (7th Edition, Accents)";

//$tbl="Mueller24";
//$f=fopen("Mueller24.koi", "rt");
//$h="English-Russian Mueller Dictionary (24th Edition)";

echo"
CREATE TABLE $tbl (
  art_id int(10) unsigned NOT NULL default '0',
  word varchar(255) NOT NULL default '',
  art_txt text NOT NULL,
  KEY ".$tbl."_syn_ndx1 (word),
  KEY ".$tbl."_syn_ndx2 (art_id)
);

";


  $s=fgets($f, 10000)."\n".fgets($f, 10000)."\n".fgets($f, 10000);

  echo "INSERT INTO $tbl VALUES (3,'$h','$tbl\\nEnglish\\nl\\nRussian\\nr');\n";
	  
  $i=10;

  while(!feof($f)){
    $s=rtrim(fgets($f, 30000));
    
    if($s!==""){
      $sp=explode("  ", $s);
      $w=$sp[0];
      $t=substr($s, strlen($w)+2);
    
      $t=str_replace("_", "", $t);
      
      for($n=1; $n<12; $n++){
        $t=str_replace("$n>", "\n$n>", $t);
      }	
	
      for($n=1; $n<6; $n++)	
        $t=str_replace($n.".", "\n".$n.".", $t);	
	
      $t=str_replace("II", "\nII", $t);		
      
      $t="$w:\n$t";
      
      echo "INSERT INTO $tbl VALUES ($i,'".mysql_escape_string($w)."','",mysql_escape_string($t),"');\n";    
      $i++;
    } 
    
  }


?>