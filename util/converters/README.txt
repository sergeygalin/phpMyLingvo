
This directory contains some sample scripts which can used to convert various
open third-party dictionary formats to PtkDic SQL dump.

THE FILES ARE PROVIDED WITHOUT ANY ADDITIONAL INFORMATION AND/OR SUPPORT.

For more information, please look into sources of the scripts.

License: Public Domain
