#!/usr/bin/php -q
<?php

  // Written by Sergey A. Galin, 2004, http://sageshome.net  
  // Removes unneeded special characters from PtkDic dictionary dumps.
  
$f=fopen($argv[1], "rt");

//echo chr(164);
//exit;

while(!feof($f)){
$line=fgets($f, 100000);

$line=str_replace(chr(164), "", $line);
while(strstr($line, "\\n');\n")!==false)
  $line=str_replace("\\n');\n", "');\n", $line);
  
for($i=1; $i<20; $i++){
  while(strstr($line, "$i):\\n")!==false)
    $line=str_replace("$i):\\n", "$i) ", $line);
}
  

while(strstr($line, "\\n\\n\\n")!==false)
  $line=str_replace("\\n\\n\\n", "\\n\\n", $line);

while(strstr($line, "  ")!==false)
  $line=str_replace("  ", " ", $line);
  
while(strstr($line, " \\n")!==false)
  $line=str_replace(" \\n", "\\n", $line);
  

echo $line;
}

fclose($f);



?>
