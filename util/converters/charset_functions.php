<?php
  // THIS FILE IS CODED IN KOI8-R.
  // PLEASE DON'T CHANGE THE ENCODING - IT WILL MAKE THE CODE WORKING IMPROPERLY!

  // This software is distributed under GNU General Public License, ver. 2
  // or higher (at your option), released by Free Software Foundation. You can
  // find text of GNU GPL at
  //   http://sageshome.net/GPL.php
  // or
  //   http://www.gnu.org/
  //
  // Copyright(C) Sergey A. Galin, 2003.

  // These are default settings. Please don't modify. Redeclare
  // the values in your config file if needed.

  // Codec to convert UTF-8 to ANSI when $enable_multibyte=true.
  // This can be set to:
  // "sg" (default) - internal codec written in PHP, slower than
  // mbstring but much more reliable (compatible with any
  // version/installation of PHP).
  // "mbstring" - use experimental PHP extension - mbstring
  // "recode" - call external program - GNU Recode. Left for
  // testing and backward compatibility only.
  $multibyte_codec="sg"; //"recode"; //"mbstring";

  // These two variables are used ONLY when $multibyte_codec set
  // to "recode".
  $recode_path="/usr/bin/recode";
  $windows_mode=false;

  // Enable raw UTf-8 output (if false, UCS characters will be output as
  // HTML entities (&#....).
  $utf8=true;


  $cyr_A_code=848+ord(WinStr('�'));
  $cyr_ya_code=848+ord(WinStr('�'));
  $cyr_YO_code=1025; $cyr_YO_1251=convert_cyr_string("�", "k", "w");
  $cyr_yo_code=1105; $cyr_yo_1251=convert_cyr_string("�", "k", "w");
  $const_latins="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  $const_wordsep=" >1234567890+=()[]{}\n\r\t\"��";
  $const_cyrl="�����ţ��������������������������".$cyr_yo_1251;
  $const_cyru="��������������������������������".$cyr_YO_1251;
  $const_cyrs=$const_cyru.$const_cyrl;

  // Taken from W3C HTML 4.01 Specification.
  $code_unicode=array(
    // 24.2 Character entity references for ISO 8859-1 characters
    'nbsp'      => 160,    'iexcl'     => 161,    'cent'      => 162,
    'pound'     => 163,    'curren'    => 164,    'yen'       => 165,
    'brvbar'    => 166,    'sect'      => 167,    'uml'       => 168,
    'copy'      => 169,    'ordf'      => 170,    'laquo'     => 171,
    'not'       => 172,    'shy'       => 173,    'reg'       => 174,
    'macr'      => 175,    'deg'       => 176,    'plusmn'    => 177,
    'sup2'      => 178,    'sup3'      => 179,    'acute'     => 180,
    'micro'     => 181,    'para'      => 182,    'middot'    => 183,
    'cedil'     => 184,    'sup1'      => 185,    'ordm'      => 186,
    'raquo'     => 187,    'frac14'    => 188,    'frac12'    => 189,
    'frac34'    => 190,    'iquest'    => 191,    'Agrave'    => 192,
    'Aacute'    => 193,    'Acirc'     => 194,    'Atilde'    => 195,
    'Auml'      => 196,    'Aring'     => 197,    'AElig'     => 198,
    'Ccedil'    => 199,    'Egrave'    => 200,    'Eacute'    => 201,
    'Ecirc'     => 202,    'Euml'      => 203,    'Igrave'    => 204,
    'Iacute'    => 205,    'Icirc'     => 206,    'Iuml'      => 207,
    'ETH'       => 208,    'Ntilde'    => 209,    'Ograve'    => 210,
    'Oacute'    => 211,    'Ocirc'     => 212,    'Otilde'    => 213,
    'Ouml'      => 214,    'times'     => 215,    'Oslash'    => 216,
    'Ugrave'    => 217,    'Uacute'    => 218,    'Ucirc'     => 219,
    'Uuml'      => 220,    'Yacute'    => 221,    'THORN'     => 222,
    'szlig'     => 223,    'agrave'    => 224,    'aacute'    => 225,
    'acirc'     => 226,    'atilde'    => 227,    'auml'      => 228,
    'aring'     => 229,    'aelig'     => 230,    'ccedil'    => 231,
    'egrave'    => 232,    'eacute'    => 233,    'ecirc'     => 234,
    'euml'      => 235,    'igrave'    => 236,    'iacute'    => 237,
    'icirc'     => 238,    'iuml'      => 239,    'eth'       => 240,
    'ntilde'    => 241,    'ograve'    => 242,    'oacute'    => 243,
    'ocirc'     => 244,    'otilde'    => 245,    'ouml'      => 246,
    'divide'    => 247,    'oslash'    => 248,    'ugrave'    => 249,
    'uacute'    => 250,    'ucirc'     => 251,    'uuml'      => 252,
    'yacute'    => 253,    'thorn'     => 254,    'yuml'      => 255,

    //24.3 Character entity references for symbols, mathematical symbols, and Greek letters
    'fnof'      => 402,
    //<!-- Greek -->
    'Alpha'     =>913,     'Beta '     =>914,     'Gamma'     =>915,
    'Delta'     =>916,     'Epsilon'   =>917,     'Zeta'      =>918,
    'Eta'       =>919,     'Theta'     =>920,     'Iota'      =>921,
    'Kappa'     =>922,     'Lambda'    =>923,     'Mu'        =>924,
    'Nu'        =>925,     'Xi'        =>926,     'Omicron'   =>927,
    'Pi'        =>928,     'Rho'       =>929,     'Sigma'     =>931,
    'Tau'       =>932,     'Upsilon'   =>933,     'Phi'       =>934,
    'Chi'       =>935,     'Psi'       =>936,     'Omega'     =>937,
    'alpha'     =>945,     'beta'      =>946,     'gamma'     =>947,
    'delta'     =>948,     'epsilon'   =>949,     'zeta'      =>950,
    'eta'       =>951,     'theta'     =>952,     'iota'      =>953,
    'kappa'     =>954,     'lambda'    =>955,     'mu'        =>956,
    'nu'        =>957,     'xi'        =>958,     'omicron'   =>959,
    'pi'        =>960,     'rho'       =>961,     'sigmaf'    =>962,
    'sigma'     =>963,     'tau'       =>964,     'upsilon'   =>965,
    'phi'       =>966,     'chi'       =>967,     'psi'       =>968,
    'omega'     =>969,     'thetasym'  =>977,     'upsih'     =>978,
    'piv'       =>982,
    //<!-- General Punctuation -->
    'bull'      =>8226,    'hellip'    =>8230,    'prime'     =>8242,
    'Prime'     =>8243,    'oline'     =>8254,    'frasl'     =>8260,
    //<!-- Letterlike Symbols -->
    'weierp'    =>8472,    'image'     =>8465,    'real'      =>8476,
    'trade'     =>8482,    'alefsym'   =>8501,    'larr'      =>8592,
    'uarr'      =>8593,    'rarr'      =>8594,    'darr'      =>8595,
    'harr'      =>8596,    'crarr'     =>8629,    'lArr'      =>8656,
    'uArr'      =>8657,    'rArr'      =>8658,    'dArr'      =>8659,
    'hArr'      =>8660,
    //<!-- Mathematical Operators -->
    'forall'    =>8704,    'part'      =>8706,    'exist'     =>8707,
    'empty'     =>8709,    'nabla'     =>8711,    'isin'      =>8712,
    'notin'     =>8713,    'ni'     =>8715,    'prod'      =>8719,
    'sum'       =>8721,    'minus'     =>8722,    'lowast'    =>8727,
    'radic'     =>8730,    'prop'      =>8733,    'infin'     =>8734,
    'ang'       =>8736,    'and'       =>8743,    'or'     =>8744,
    'cap'       =>8745,    'cup'       =>8746,    'int'       =>8747,
    'there4'    =>8756,    'sim'       =>8764,    'cong'      =>8773,
    'asymp'     =>8776,    'ne'     =>8800,    'equiv'     =>8801,
    'le'        =>8804,    'ge'     =>8805,    'sub'       =>8834,
    'sup'       =>8835,    'nsub'      =>8836,    'sube'      =>8838,
    'supe'      =>8839,    'oplus'     =>8853,    'otimes'    =>8855,
    'perp'      =>8869,    'sdot'      =>8901,
    //<!-- Miscellaneous Technical -->
    'lceil'     =>8968,    'rceil'     =>8969,    'lfloor'    =>8970,
    'rfloor'    =>8971,    'lang'      =>9001,    'rang'      =>9002,
     //<!-- Geometric Shapes -->
    'loz'       =>9674,    'spades'    =>9824,    'clubs'     =>9827,
    'hearts'    =>9829,    'diams'     =>9830,

    //24.4 Character entity references for markup-significant and internationalization characters
    'quot'      => 34,     'amp'       => 38,     'lt'     => 60,
    'gt'        => 62,     'OElig'     => 338,    'oelig'     => 339,
    'Scaron'    => 352,    'scaron'    => 353,    'Yuml'      => 376,
    'circ'      => 710,    'tilte'     => 732,    'ensp'      => 8194,
    'emsp'      => 8195,   'thinsp'    => 8201,   'emsp'      => 8195,
    'thinsp'    => 8201,   'zwnj'      => 8204,   'zwj'       => 8205,
    'lrm'       => 8206,   'rlm'       => 8207,   'ndash'     => 8211,
    'mdash'     => 8212,   'lsquo'     => 8216,   'rsquo'     => 8217,
    'sbquo'     => 8218,   'ldquo'     => 8220,   'rdquo'     => 8221,
    'bdquo'     => 8222,   'dagger'    => 8224,   'Dagger'    => 8225,
    'permil'    => 8240,   'lsaquo'    => 8249,   'rsaquo'    => 8250,
    'euro'      => 8364
  );

  // Table of conversion from 8 bit to UNICODE of transcription
  // characters. Taken from JaLingo Project.
  $ipa2unicode=array(
    0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, //   8
    0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, //  16
    0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, //  24
    0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, //  32
    0x0020, 0x030B, 0x0131, 0x0304, 0x0300, 0x030F, 0x030C, 0x02BC, //  40
    0x0306, 0x0303, 0x030A, 0x031F, 0x002C, 0x0324, 0x002E, 0x002F, //  48
    0x0330, 0x0318, 0x0319, 0x031D, 0x031E, 0x032A, 0x033B, 0x031C, //  56
    0x0325, 0x032F, 0x02E1, 0x029F, 0x207F, 0x0320, 0x02D1, 0x0294, //  64

    0x0301, 0x0251, 0x03B2, 0x0063, 0x00F0, 0x025B, 0x0264, 0x0262,
    0x02B0, 0x026A, 0x02B2, 0x029C, 0x026E, 0x0271, 0x014B, 0x00F8,
    0x0275, 0x00E6, 0x027E, 0x0283, 0x03B8, 0x028A, 0x028B, 0x02B7,
    0x03C7, 0x028F, 0x0292, 0x005B, 0x005C, 0x005D, 0x0302, 0x0308,
    0x0329, 0x0061, 0x0062, 0x0063, 0x0064, 0x0065, 0x0066, 0x0261,
    0x0068, 0x0069, 0x006A, 0x006B, 0x006C, 0x006D, 0x006E, 0x006F,
    0x0070, 0x0071, 0x0072, 0x0073, 0x0074, 0x0075, 0x0076, 0x0077,
    0x0078, 0x0079, 0x007A, 0x0280, 0x031A, 0x027D, 0x033D, 0x007F,

    0x02E9, 0x0252, 0x0258, 0x0361, 0x2016, 0x02E5, 0x02E5, 0x0298,
    0x030B, 0x030B, 0x02E5, 0x2191, 0x0250, 0x0254, 0x01C0, 0x0301,
    0x0301, 0x02E6, 0x01C1, 0x0304, 0x0304, 0x02E7, 0x007C, 0x01C3,
    0x0300, 0x0300, 0x02E8, 0x2193, 0x01C2, 0x030F, 0x030F, 0x02E9,
    0x0020, 0x030A, 0x031E, 0x031D, 0x032C, 0x0325, 0x0339, 0x0282,
    0x0279, 0x0260, 0x0319, 0x0259, 0x0289, 0x0320, 0x0268, 0x0276,
    0x033A, 0x031F, 0x0274, 0x02E4, 0x028E, 0x026F, 0x0020, 0x0020,
    0x0278, 0x02A2, 0x0253, 0x032F, 0x0330, 0x0290, 0x006A, 0x0153,

    0x0295, 0x0318, 0x026C, 0x028C, 0x0263, 0x0020, 0x029D, 0x02CC,
    0x02C8, 0xF180, 0x200A, 0xF181, 0x2197, 0x2198, 0x025C, 0x025E,
    0x0324, 0x033C, 0x0281, 0x027B, 0xF182, 0x02DE, 0x002D, 0x0284,
    0x02E7, 0x02E7, 0x030B, 0x0301, 0x0304, 0x0300, 0x030F, 0x0302,
    0x030C, 0x0306, 0x0303, 0x028D, 0x027A, 0x0270, 0x0302, 0x0265,
    0x02E9, 0x0302, 0x0256, 0x0257, 0x02E0, 0x203F, 0x0267, 0x025F,
    0x0127, 0x026D, 0x026B, 0x030C, 0x030C, 0x0299, 0x0268, 0x0273,
    0x0272, 0x003A, 0x0266, 0x02A1, 0x0291, 0x029B, 0x0255, 0x0288
  );

  // Table to simplify charset by removing diacritic symbols.
  // Thanks to Alexander Goriachev AKA Thorn for this table!
  // See also: SimplifyWriting().
  $simplify_writing_table=array(
    // Uppercase Letters
    192,65,  // &Agrave - ������� � � ���������
    193,65,  // &Aacute - ������� � � ���������
    194,65,  // &Acirc - ������� � � "�������"
    195,65,  // &Atilde - ������� � � �������
    196,65,  // &Auml - ������� �-������
    197,65,  // &Aring - ������� � � �������
    198,65, //,69,  // &AElig - ������� �E
    199,67,  // &Ccedil - ������� C �����
    200,69,  // &Egrave - ������� E � ���������
    201,69,  // &Eacute - ������� E � ���������
    202,69,  // &Ecirc - ������� E � "�������"
    203,69,  // &Euml - ������� E-������
    204,73,  // &Igrave - ������� I � ���������
    205,73,  // &Iacute - ������� I � ���������
    206,73,  // &Icirc - ������� I � "�������"
    207,73,  // &Iuml - ������� I-������
    208,68,  // &ETH - ������� Eth
    209,78,  // &Ntilde - ������� N � �������
    210,79,  // &Ograve - ������� O � ���������
    211,79,  // &Oacute - ������� O � ���������
    212,79,  // &Ocirc - ������� O � "�������"
    213,79,  // &Otilde - ������� O � �������
    214,79,  // &Ouml - ������� O-������
    215,215, // &times - ���� ���������
    216,79,  // &Oslash - ������� O �����������
    217,85,  // &Ugrave - ������� U � ���������
    218,85,  // &Uacute - ������� U � ���������
    219,85,  // &Ucirc - ������� U � "�������"
    220,85,  // &Uuml - ������� U-������
    221,89,  // &Yacute - ������� Y � ���������
    222,80,  // &THORN - ������� Thorn  P
    223,66,  // &szlig - ������ B

    // Lowercase Letters
    224,97,  // &agrave - ��������� a � ���������
    225,97,  // &aacute - ��������� a � ���������
    226,97,  // &acirc - ��������� a � "�������"
    227,97,  // &atilde - ��������� a � �������
    228,97,  // &auml - ��������� a-������
    229,97,  // &aring - ��������� a c �������
    230,97, //,101,  // &aelig - �������oe ae
    231,99,  // &ccedil - �������a� �-�����
    232,101,  // &egrave - ��������� � � ���������
    233,101,  // &eacute - ��������� � � ���������
    234,101,  // &ecirc - ��������� � � "�������"
    235,101,  // &euml - ��������� �-������
    236,105,  // &igrave - ��������� i � ���������
    237,105,  // &iacute - ��������� i � ���������
    238,105,  // &icirc - ��������� i � "�������"
    239,105,  // &iuml - ��������� i-������
    240,100,  // &eth - �������oe eth d
    241,110,  // &ntilde - �������a� n � �������
    242,111,  // &ograve - ��������� o � ���������
    243,111,  // &oacute - ��������� o � ���������
    244,111,  // &ocirc - ��������� o � "�������"
    245,111,  // &otilde - ��������� o � �������
    246,111,  // &ouml - ��������� o-������
    247,247,  // &divide - ���� �������
    248,111,  // &oslash - ��������� o �����������
    249,117,  // &ugrave - ��������� u � ���������
    250,117,  // &uacute - ��������� u � ���������
    251,117,  // &ucirc - ��������� u � "�������"
    252,117,  // &uuml - ��������� u-������
    253,121,  // &yacute - ��������� y � ���������
    254,112,  // &thorn - ��������� Thorn p
    255,121   // &yuml - ��������� y-������
  );


//******************************************************************************
//
// Character type detection functions.
//
//******************************************************************************

  function IsLatin($c){
    return ($c>='A' && $c<'Z') || ($c>='a' && $c<'z');
  }

  function IsCyrillic($c){
    global $const_cyrs;
    return strpos($const_cyrs, $c)!==false;
  }

  function IsSeparator($c){
    global $const_wordsep;
    return strpos($const_wordsep, $c)!==false;
  }

  // This function checks whether the charater is ASCII or Cyrillic letter.
  // It assumes that all [possible] characters with code>=128 (which are
  // not explicitly word separators) are letters!
  // This is always true for example for dictionaries (and for most text
  // documents).
  function IsLetter($c){
    return IsLatin($c) || (ord($c)>127 && !IsSeparator($c));
  }

//******************************************************************************
//
// Multi-byte string functions
//
//******************************************************************************

  // Calclulate number of bytes in UTF-8 character by its first byte.
  // For bad starting byte, returns 1.
  function UTF8NumBytes($code){
    if(($code & 0xE0)==0xC0) return 2;
    if(($code & 0xF0)==0xE0) return 3;
    if(($code & 0xF8)==0xF0) return 4;
    if(($code & 0xFC)==0xF8) return 5;
    if(($code & 0xFE)==0xFC) return 6;
    return 1;
  }

  // Convert UTF-8 sequence to UCS-4 character code.
  function UTF8Ch_UCS4b($s, $nb){
    $c=ord($s{0}) & (0xFF>>($nb+1));
    for($i=1; $i<$nb; $i++){
      $c<<=6;
      $c|=ord($s{$i}) & 0x3F;
    }
    return ($c==0)?ord('?'):$c;
  }

  // Convert UCS-4 character code to UTF-8 string.
  function UCS4bCh_UTF8($c){
    if($c<0x80) return chr($c); // 7-bit character
    // Look how many bits are used in the character.
    $bi=8; $t=$c>>8;
    while($t>0){ $bi++; $t>>=1; }
    $tr=floor($bi/6); // number of fully used following bytes
    $fi=$bi-$tr*6; // number of bits left (== $bi%6?)
    $nb=$tr+1; // number of required bytes
    if($nb+1+$fi>8){
      $nb++; // additional byte needed
      $tr++; // this means one more trailing byte
    }
    $r="";
    for($i=0; $i<$tr; $i++){
      $r=chr(0x80+($c&0x3F)).$r;
      $c>>=6;
    }
    $r=chr(((0xFF>>(8-$nb))<<(8-$nb))+$c).$r;
    return $r;
  }

  // Convert UTF-8 string to UTF-16 string.
  function UTF8_UTF16($s){
    $r="";
    $i=0;
    while($i<strlen($s)){
      if(ord($s{$i})<0x80){
        $r.=chr(0).$s{$i++};
      }else{
        $nb=UTF8NumBytes(ord($s{$i}));
        $c=UTF8Ch_UCS4b(substr($s, $i, $nb), $nb);
        if($c<=0xFFFF) $r.=chr($c>>8).chr($c & 0xFF);
        else $r.=chr(0).'?';
        $i+=$nb;
      }
    }
    return $r;
  }




  function WinStr($str){ return convert_cyr_string($str, "k", "w"); }



//******************************************************************************
//
// High-level charset conversion functions which use global variable
// $utf8 to detect type of output:
// if $utf8==false then output is encoded into HTML entities, otherwise
// output is UTF-8.
//
//******************************************************************************

  function HTMLExtCh($c){
    global $utf8;
    if(!$utf8 || $c<256) return sprintf("&#%d;", $c);
    else return UCS4bCh_UTF8($c);
  }

  // This function returns UNICODE representation of character $ch
  // in Cyrillic mode (in &# notation). I.e. it converts KOI8-R
  // character into HTML entity-coded UTF representation.
  function CyrChUTF($ch){
    global $const_cyrs, $cyr_YO_code, $cyr_yo_code,
      $cyr_YO_1251, $cyr_yo_1251;
    if(ord($ch)<128) return $ch;
    else if(strstr($const_cyrs, $ch)===false) return HTMLExtCh(ord($ch));
    else if($ch=="�" || $ch==$cyr_yo_1251) return HTMLExtCh($cyr_yo_code);
    else if($ch=="�" || $ch==$cyr_YO_1251) return HTMLExtCh($cyr_YO_code);
    else return HTMLExtCh(848+ord(WinStr($ch{0})));
  }

  function LatChUTF($ch){
    if(ord($ch)<128) return $ch;
    else return HTMLExtCh(ord($ch));
  }

  function TranscrChUTF($ch){
    global $ipa2unicode;
    return HTMLExtCh($ipa2unicode[ord($ch)]);
  }

  function CyrStrUTF($str){
    $result="";
    for($i=0; $i<strlen($str); $i++) $result.=CyrChUTF($str{$i});
    return $result;
  }

  function LatStrUTF($str){
    $result="";
    for($i=0; $i<strlen($str); $i++) $result.=LatChUTF($str{$i});
    return $result;
  }


  // This function processes ANSI HTML text and converts extended characters
  // (code>=128) to UNICODE representation. It also supports "smart" charset
  // detection so it can correctly process mixed European and Cyrillic
  // encodings.
  function ProcessEncoding($s, $transcription){
    global $const_wordsep;
    $result="";
    $i=0;
    while($i<strlen($s)){
     if(!IsLetter($s{$i})){
       if($transcription && $s{$i}=='['){
         $result.='[';
         $i++;
         while($i<strlen($s) && $s{$i}!=']'){
           $result.=TranscrChUTF($s[$i]);
           $i++;
         }
         $i--;
       }else
         $result.=LatChUTF($s{$i});
     }else{
       $cyrs=true;
       $j=$i;
       while($j<strlen($s) && IsLetter($s{$j})){
         if(!IsCyrillic($s{$j})){  //if(ord($s{$j})<128){
           $cyrs=false;
           break;
         }
         $j++;
       }
       if(!$cyrs)
         while($i<strlen($s) && IsLetter($s{$i})) $result.=LatChUTF($s{$i++});
       else
         while($i<strlen($s) && IsLetter($s{$i})) $result.=CyrChUTF($s{$i++});
       $i--;
     }
     $i++;
    }

    // Performing "smart" replacements.
    $result=str_replace(CyrStrUTF("\n� "), LatStrUTF("\n� "), $result);
    $result=str_replace(CyrStrUTF(" � ") , LatStrUTF(" � "), $result);
    $result=str_replace(CyrStrUTF("'�")  , LatStrUTF("'�"), $result);
    $result=str_replace(CyrStrUTF(" �?") , LatStrUTF(" �?"), $result);
    $result=str_replace(CyrStrUTF(" �!") , LatStrUTF(" �!"), $result);
    $result=str_replace(CyrStrUTF(" �.") , LatStrUTF(" �."), $result);
    $result=str_replace(CyrStrUTF("-�-") , LatStrUTF("-�-"), $result);

    return $result;
  }


  // This function accepts [decimal] code of charater and returns
  // its character (single-byte) representation in ANSI charset
  // (ISO or KOI).
  function UTFCh2Ansi($code){
    // 848+ord(WinStr($ch{0}))
    global $cyr_A_code, $cyr_ya_code, $cyr_YO_code, $cyr_yo_code;
    if($code>=$cyr_A_code && $code<=$cyr_ya_code)
      return convert_cyr_string(chr($code-848), "w", "k");
    else if($code==$cyr_yo_code) return "�";
    else if($code==$cyr_YO_code) return "�";
    else if($code<256) return chr($code);
    else return "[UnknownChr#$code]";
  }

  // This function converts input containing UCS characters
  // coded in numberic or named entities.
  function StripUTF($s){
    global $code_unicode;
    $i=0;
    $result="";
    while($i<strlen($s)){
     // UNICODE in &# notation
     if($s{$i}=='&' && $s{$i+1}=='#'){
       $i+=2;
       $code="";
       while($s{$i}>='0' && $s{$i}<='9') $code.=$s{$i++};
       $result.=UTFCh2Ansi($code);
       $i++;
     // Entities (&#scarot;...)
     }else if($s{$i}=='&' && IsLatin($s{$i+1})){
       $i++;
       $code="";
       while(IsLatin($s{$i})) $code.=$s{$i++};
       if(isset($code_unicode[$code]))
         $result.=UTFCh2Ansi($code_unicode[$code]);
       else
         //"[UnknownChr#$code]";
         $result.="&#38;$code";
       $i++;
     }else{
       $result.=$s{$i++};
     }
    }
    return $result;
  }

  function PrintFileUTF($fn){
    $f=fopen($fn, "rt");
    $s=fread($f, 1000000);
    echo ProcessEncoding($s, true);
    fclose($f);
  }

  function SGToLower($s){
    global $const_cyru, $const_cyrl;
    $ret="";
    for($i=0; $i<strlen($s); $i++)
      if(($pos=strpos($const_cyru, $s{$i}))!==false)
     $ret.=$const_cyrl{$pos};
      else
     $ret.=strtolower($s{$i});
    return $ret;
  }

  function SGToUpper($s){
    global $const_cyru, $const_cyrl;
    $ret="";
    for($i=0; $i<strlen($s); $i++)
      if(($pos=strpos($const_cyrl, $s{$i}))!==false)
     $ret.=$const_cyru{$pos};
      else
     $ret.=strtoupper($s{$i});
    return $ret;
  }

  function SGUCFirst($s){
    $inword=0;
    $ret="";
    for($i=0; $i<strlen($s); $i++){
      if(IsLetter($s{$i})){
     $inword++;
     if($inword==1) $ret.=SGToUpper($s{$i});
     else $ret.=SGToLower($s{$i});
      }else{
     $inword=0;
     $ret.=$s{$i};
      }
    }
    return $ret;
  }

  function SGPossibleRegs($s, $utf8){
    $ret=array("DEF"=>$s);
    if (!$utf8) {
      $up=SGToUpper($s);
      $lo=SGToLower($s);
      $uf=SGUCFirst($s);
    } else {
      $up=mb_strtoupper($s);
      $lo=mb_strtolower($s);
      $uf=mb_convert_case($s, MB_CASE_TITLE);
    }
    //echo "($s,$up,$lo,$uf)";
    if(array_search($up, $ret, true)==false) $ret["UP"]=$up;
    if(array_search($lo, $ret, true)==false) $ret["LO"]=$lo;
    if(array_search($uf, $ret, true)==false) $ret["UF"]=$uf;
    //foreach($ret as $r) echo $r, "*"; echo "//";
    return $ret;
  }

  function CountChar($str, $char){
    //$ret=0;
    //for($i=0; $i<strlen($str); $i++) if($str{$i}==$char) $ret++;
    //return $ret;
    $cnt=count_chars($str, 0);
    return $cnt[ord($char)];
  }

  // Convert UTF-8 string to ANSI using mapping:
  // Latin characters -> ISO-8859-1
  // Cyrillic characters -> KOI8-R
  function UTF8_to_ANSI($query){
    global $multibyte_codec, $recode_path, $windows_mode;
    if($multibyte_codec=="sg"){
      $r="";
      $i=0;
      while($i<strlen($query)){
        if(ord($query{$i})<0x80){
          $r.=$query{$i++};
        }else{
          $nb=UTF8NumBytes(ord($query{$i}));
          $r.=UtfCh2Ansi(UTF8Ch_UCS4b(substr($query, $i, $nb), $nb));
          $i+=$nb;
        }
      }
      return $r;
    }else if($multibyte_codec=="recode"){
      if(!$windows_mode){
        $cmd=addcslashes($query, "\"!$");
        $u16=`echo "$cmd" | $recode_path UTF-8..UTF-16`;
        $trail=3;
      }else{
        $u16=`echo $query | $recode_path UTF-8..UTF-16`;
        $trail=5;
      }
      $qry2="";
      for($i=2; $i<strlen($u16)-$trail; $i+=2)
        $qry2.=UtfCh2Ansi( (ord($u16{$i})<<8) + ord($u16{$i+1}) );
      return $qry2; // rtrim($qry2);
    }else{
      // Detect which 8-bit encoding is more suitable for $query
      // and return $query converted to ANSI.
      $qry=mb_convert_encoding($query, "ISO-8859-1", "UTF-8");
      $qry2=convert_cyr_string(mb_convert_encoding($query, "ISO-8859-5", "UTF-8"), "i", "k");
      $qryc=CountChar($qry, '?');
      $qryc2=CountChar($qry2, '?');
      if($qryc<$qryc2) return $qry; else return $qry2;
    }
  }

  // This function removes diacritic symbols from latin string in 8-bit
  // European encoding.
  function SimplifyWriting($ansi){
    global $simplify_writing_table;
    $ret=""; $l=strlen($ansi);
    $minconvert=$simplify_writing_table[0];
    for($i=0; $i<$l; $i++)
      if(ord($ansi[$i])<$minconvert)
        $ret.=$ansi[$i];
      else
        $ret.=$simplify_writing_table[(ord($ansi[$i])-$minconvert)*2+1];
    return $ret;
  }


?>
