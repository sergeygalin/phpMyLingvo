#!/usr/bin/php -q
<?php

  // Written by Sergey A. Galin, 2004, http://sageshome.net  

  // THIS SCRIPT IS AN UNUSABLE SKETCH 
  // (written before first usable XDXF specs releases)


  $path_to_phpmylingvo="../";
  $xdxf_version="0.1";
  $save2xdxf_version="PtkDic 2 XDXF Converter v. 0.1";

  include_once($path_to_phpmylingvo."charset_functions.php");
  include_once($path_to_phpmylingvo."config.php");
  include_once($path_to_phpmylingvo."functions.php");
  mysql_select_db($database) or
    die("Cannot select database $database: ".mysql_error());


  $process_charset=true;
  $utf8=true;

  $languages=array(
    "en"=>"English",
    "ru"=>"Russian",
    "it"=>"Italian",
    "de"=>"Deutch",
    "sp"=>"Spanish"
  );

  if($argc<2){
    echo "Usage: save2xml dictionary_name\n";
    exit;
  }else{
    $dict=$argv[1];
  }

  //echo "<!-- Source Dictionary: $dict -->\n";

  function GetRowById($dict, $id){
    global $dict;
    $query="SELECT * FROM $dict WHERE art_id=$id";
    $result=mysql_query($query) or die("SQL error: ".mysql_error());
    $ret=mysql_fetch_array($result);
    mysql_free_result($result);
    return $ret;
  }

  $info=GetRowById($dict, 3);
  $dicttype=explode("\n", $info["art_txt"]);
  $lngfrom=$dicttype[1]; $lngfrom2=strtolower(substr($lngfrom, 0, 2));
  $lngto=$dicttype[3]; $lngto2=strtolower(substr($lngto, 0, 2));


  echo
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
<xdxf>
  <xdxf-version value=\"$xdxf_version\" />
  <dictionary-info>
    <dictionary-id value=\"$dict\" />
    <dictionary-name value=\"$info[word]\" />
    <dictionary-language-from value=\"$lngfrom2\" longdesc=\"$lngfrom\" />
    <dictionary-language-to value=\"$lngto2\" longdesc=\"$lngto\" />
    <dictionary-site value=\"http://xdxf.sourceforge.net/\" />
    <dictionary-generator value=\"$save2xdxf_version\" />
  </dictionary-info>
  <articles>
";

  $query="SELECT * FROM $dict WHERE art_id>=10 ORDER BY art_id";
  $result=mysql_query($query) or die("SQL error: ".mysql_error());
  $cnt=0;
  while($row=mysql_fetch_array($result)){
    $cnt++;

    // art_id, word, art_txt
    $title=ProcessEncoding($row["word"], false);
    $text=PrepareArticle($row, $row["word"],
      !$dic["disable_transcription"]);

    echo "  <article id=\"$row[art_id]\" title=\"$title\">$text</article>\n\n";

//if($cnt>4) break;
    set_time_limit(30);
  }
  mysql_free_result($result);

  echo "
  </articles>
</xdxf>
";

?>
