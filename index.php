<?php
  // This software is distributed under GNU General Public License, ver. 2
  // or higher (at your option), released by Free Software Foundation. You can
  // find text of GNU GPL at
  //   http://sageshome.net/GPL.php
  // or
  //   http://www.gnu.org/
  //
  // Copyright(C) Sergey A. Galin, 2003-2004.

  // PLEASE DON'T MODIFY THIS FILE UNLESS
  // YOU WANT TO ALTER SOFTWARE FUNCTIONING.
  // See config.php for all configuration options.
  // See welcome.inc.html for Welcome text.


  $version_file=fopen("version.txt", "r");
  $version=trim(fgets($version_file, 100));
  fclose($version_file);
  
  require_once("charset_functions.php");
  require_once("config.php");
  require_once("functions.php");
  require_once("formvars.php");
  OldCookies("phpdictops");


  // Below is a code which don't has to be edited unless you want to
  // modify the program.

  /*
    Original PtkDic dictionary format:

    CREATE TABLE eng_eng_main (
    art_id int(10) unsigned NOT NULL default '0',
    word varchar(255) NOT NULL default '',
    art_txt text NOT NULL,
    KEY eng_eng_main_ndx1 (word),
    KEY eng_eng_main_ndx2 (art_id)
    );

    Records where art_id<10 are reserved and should not be included
    into search results.
    Dictionary title always located in field 'word' by art_id=3.
  */


  $search_results=0;

  // ***************************************
  // Load list of available dictionaries into $diclist[]
  // ***************************************

  LoadDicList();

  // ***************************************
  // Load data from cookie.
  // ***************************************

  $phpdic_old_cookie=@unserialize(@base64_decode($_COOKIE["phpdictops"])); // +0.9.3
  $phpdic_new_cookie=array(); // +0.9.3

  // ***************************************
  // Form Vars compatibility
  // ***************************************

  $formvars=array();
  for($i=0; $i<sizeof($diclist); $i++)
    array_push($formvars, "use_".$diclist[$i]["Name"]);
  array_push($formvars, "query");
  array_push($formvars, "get");
  array_push($formvars, "partial");
  array_push($formvars, "anycase");
  array_push($formvars, "fulltext");
  array_push($formvars, "utf8query");
  array_push($formvars, "default_select_all");
  OldFormVarsA($formvars);
  $get=Get("get");
  $query=Get("query");
  $partial=Get("partial");
  $anycase=Get("anycase");
  $fulltext=Get("fulltext");

  if( intval(Get("utf8query")) ) {
    if ($process_charset)
      $query = UTF8_to_ANSI( $query );
  }

  // Save $query value

  $query_asis=$query;

  // ***************************************
  // Set use_dictionary variables
  // ***************************************
  
  $defSelectAll = intval(Get('default_select_all'));
  if( !$defSelectAll )
    $defSelectAll = $default_dictionary_selection;

  for($i=0; $i<sizeof($diclist); $i++){
    $varn="use_".$diclist[$i]["Name"];
    ${$varn}=$_GET[$varn];
    $cookval=isset($phpdic_old_cookie[$varn])?$phpdic_old_cookie[$varn]:""; // +0.9.3
    // $debug.="$coon=$cookval; ";
    if(!empty($get)){ // Form submitted, set all cookies and vars accordingly the form
      if(isset(${$varn})){
        ${$varn}=1;
        $phpdic_new_cookie[$varn]=1; //+0.9.3
      }else{
        ${$varn}=0;
        $phpdic_new_cookie[$varn]=0; //+0.9.3
      }
    }else{ // form is not submitted, load everything from cookies
      if(strlen($cookval))
        ${$varn}=$cookval;
      else{
        // No cookie as well - doing default selection
        ${$varn} = $defSelectAll;
      }
      $phpdic_new_cookie[$varn]=${$varn}; // +0.9.3
    }
  }


  // ***************************************
  // Processing cookies/submitted values for other options
  // ***************************************

  // partial, fulltext, anycase
  if(empty($get)){ // restore from cookies
    $partial=$phpdic_old_cookie["opt_partial"];
    $fulltext=$phpdic_old_cookie["opt_fulltext"];
    $anycase=$phpdic_old_cookie["opt_anycase"];
  }else{
    if($trim_input) $query=trim($query);
    //$query=stripcslashes($query); - don't required since we use Get() to read input
    if ($charset=="UTF-8") {
      if ($process_charset) {
        $query=UTF8_to_ANSI($query);
      }
    } else {
      if($input_unicode) $query=StripUTF($query);
    }
  }
  $html_query=htmlspecialchars($query);
  if($process_charset) $html_query=ProcessEncoding($html_query, false);

  // Update Search Options in cookie
  $phpdic_new_cookie["opt_partial"]=$partial;
  $phpdic_new_cookie["opt_fulltext"]=$fulltext;
  $phpdic_new_cookie["opt_anycase"]=$anycase;


  // ***************************************
  // Load data to cookie.
  // ***************************************
  if(!isset($cookie_path)) $cookie_path="/";
  setcookie("phpdictops", base64_encode(serialize($phpdic_new_cookie)), $cookie_expire, $cookie_path);

//echo $debug;

  // Converts article $art (table row) found by request $search
  // into nicely formatted HTML.

  function ArtContains($row, $query){
    $text=SGToUpper($row["word"]." ".$row["art_txt"]);
    $q=SGToUpper($query);
    return strpos($text, $q)!==false;
  }

  // Search $dic for $query, options. Returns number of articles found.
  function SearchDict($dic, $query, $substring, $fulltext, $anycase){
    global $max_search_results, $search_results, $fix_koi_search_problem, $db, $charset;
    // Getting results for index search
    $gotids="";
    $output="";
    $ret=0;
    $qmsu=$db->quote($query);
    if(!empty($anycase)){
      $cases=SGPossibleRegs($query, $charset==='UTF-8');
      $cond="";
      foreach($cases as $cas){
        if($cond!="") $cond.=" OR ";
        if(!empty($substring))
          $cond.=$db->IsSubstringFunction($db->quote($cas), "word");
        else
          $cond.="word=".$db->quote($cas);
      };
      $q="SELECT * FROM $dic[Name] WHERE (art_id>=10) AND ($cond) ORDER BY word";
    }else
      $q="SELECT * FROM $dic[Name] WHERE (art_id>=10) AND ".
        ((!empty($substring))?
          $db->IsSubstringFunction($qmsu, "word"): "word=$qmsu").
          " ORDER BY art_id";
    // echo "[[[[[[[[[[ ", htmlspecialchars($q), " ]]]]]]]]]] , fix_koi: $fix_koi_search_problem<br>\n";
    $res=$db->query($q);
    if (DB::isError($res))
      die ("Error: ".$res->getMessage()."<br />".$q);
    while($row=$res->fetchrow()){
      if (!$fix_koi_search_problem || ArtContains($row, $query)){
        $ret++;
        $output.=PrepareArticle($row, $query, !$dic["disable_transcription"]);
        if(strlen($gotids)>0) $gotids.=" OR ";
        $gotids.="art_id=".$row["art_id"];
        if(++$search_results>=$max_search_results) break;
      }
    }
    $res->free();
    if(strlen($output))
      echo "<p class=\"art_dic\"><small class=\"art_dic\">",
           htmlspecialchars($dic["Desc"])."</small></p>\n",
           $output, "\n";
    $output="";
    // Most terrible (for computer) - full-text search
    if($search_results<$max_search_results && !empty($fulltext)){
      if(!empty($anycase)){
        $cond="";
        foreach($cases as $cas){
          if($cond!="") $cond.=" OR ";
          $cond.=$db->IsSubstringFunction($db->quote($cas), "art_txt");
        };
      }else
        $cond=$db->IsSubstringFunction($db->quote($query), "art_txt");

      $q="SELECT * FROM $dic[Name] WHERE art_id>=10 AND ".
        ((strlen($gotids)>0)? "NOT($gotids) AND ":"").
        " ($cond) ORDER BY art_id";
//echo $q;
        $res=$db->query($q);
        if (DB::isError($res)) die ("Error: ".$res->getMessage());

      while($row=$res->fetchrow()){

        if(!$fix_koi_search_problem || ArtContains($row, $query)){
          $ret++;
          $output.=PrepareArticle($row, $query, !$dic["disable_transcription"]);
          if(++$search_results>=$max_search_results) break;
        }
      }
      $res->free();
      if(strlen($output))
        echo "<p class=\"art_dic\"><small class=\"art_dic\">",
             htmlspecialchars($dic["Desc"])." - full-text search</small></p>\n",
             $output, "\n";
    }
    return $ret;
  }

  header("Content-Type: text/html; charset=".$charset);
  if($allow_compression)
    @ob_start("ob_gzhandler");
  echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php
    if(!empty($get) && strlen($query))
      echo $html_query, " - ";
    echo $title;
  ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $charset; ?>" />
  <meta name="Keywords" content="phpMyLingvo PHPDic PHP Dic MySQL PtkDic GtkDic JaLingvo dictionary glossary GNU GPL FSF" />
  <meta name="Description" content="<?php echo $description; ?>" />
  <meta name="Author" content="Sergey A. Galin, http://sageshome.net" />
  <link rel="search" type="application/opensearchdescription+xml" href="opensearch.php" title="<?php echo $title; ?>" />
  <link rel="StyleSheet" type="text/css" href="style.css" />
  <script type="text/javascript"><!--
function SetAll(){
<?php
  for($i=0; $i<sizeof($diclist); $i++)
    echo "document.mainform.use_", $diclist[$i]["Name"], ".checked=true;\n";
?>
}
function ClearAll(){
<?php
  for($i=0; $i<sizeof($diclist); $i++)
    echo "document.mainform.use_", $diclist[$i]["Name"], ".checked=false;\n";
?>
}
    <?php
      // Generating code for Dictionary Group buttons:
      $dictypes=array();
      $dictypesS=array();
      $diccmds=array();
      $diccmdsS=array();
      $dicbuttons=array();
      $dicbuttonsS=array();
      foreach($diclist as $dict){
        $dfrom=strtoupper(substr($dict["From"], 0, 3));
        $dto=strtoupper(substr($dict["To"], 0, 3));

        $dictyp=$dfrom." -> ".$dto;
        if($dict["From"]<$dict["To"]) $dictypS=$dfrom." <-> ".$dto;
        else                          $dictypS=$dto." <-> ".$dfrom;
        // do we already have such type in the list?
        $previdx=array_search($dictyp, $dictypes, true);
        if(strlen($previdx)){
          $diccmds[$previdx].="document.mainform.use_".$dict["Name"].".checked=true;\n";
          $i=$previdx;
        }else{
          array_push($dictypes, $dictyp);
          $i=array_push($diccmds, "document.mainform.use_".$dict["Name"].".checked=true;\n")
            -1;
        }
        $previdxS=array_search($dictypS, $dictypesS, true);
        if(strlen($previdxS)){
          $diccmdsS[$previdxS].="Group$i();";
        }else{
          array_push($dictypesS, $dictypS);
          array_push($diccmdsS, "Group$i();");
        }
      }
      // getting list of languages
      $languages=array();
      foreach($diclist as $dict){
        $previdx=array_search($dict["From"], $languages, true);
        if(!strlen($previdx)) array_push($languages, $dict["From"]);
        $previdx=array_search($dict["To"], $languages, true);
        if(!strlen($previdx)) array_push($languages, $dict["To"]);
      }
      sort($languages, SORT_STRING);
      $languagecmds=array();
      foreach($languages as $lang){
        $cmd=""; $lng3=strtoupper(substr($lang, 0, 3));
        for($j=0; $j<sizeof($dictypes); $j++)
          if(strstr($dictypes[$j], $lng3)!==false)
            $cmd.="Group$j();";
        $cmd="<a href=\"javascript:ClearAll();$cmd\" ".
        "title=\"Click to select all dictionaries containing $lang language\">".
        htmlspecialchars($lang)."</a>";
        array_push($languagecmds, $cmd);
      }
      echo "\n";
      for($i=0; $i<sizeof($dictypes); $i++){
        echo "function Group$i(){\n$diccmds[$i]}\n";
        $dicbuttons[$dictypes[$i]]="<a href=\"javascript:ClearAll();Group$i();\" ".
        "title=\"Click to select all dictionaries for $dictypes[$i]\">".
        str_replace(' ', '&nbsp;', htmlspecialchars($dictypes[$i]))."</a>";
      }
      ksort($dicbuttons, SORT_STRING);
      for($i=0; $i<sizeof($dictypesS); $i++){
        $dicbuttonsS[$dictypesS[$i]]="<a href=\"javascript:ClearAll();$diccmdsS[$i]\" ".
        "title=\"Click to select all dictionaries for $dictypesS[$i]\">".
        str_replace(' ', '&nbsp;', htmlspecialchars($dictypesS[$i]))."</a>";
      }
      ksort($dicbuttonsS, SORT_STRING);
    ?>

    function DictInfoPopup(){
      var w=screen.width;
      var h=screen.height;
      var ww=550;
      var wh=350;
      dictinfo=window.open("", "dictinfo",
        "width="+ww+",height="+wh+",screenX="+(w-ww)/2+",screenY="+(h-wh)/2+
        ",alwaysRaised=1,onTop=1,dependent=1,status=1,resizable=1");
    }
    //-->
  </script>
</head>
<body>

<h1><?php echo $title; ?></h1>
<table width="100%" class="maintable" cellspacing="0" cellpadding="0">
  <form name="mainform" action="<?php echo $PHP_SELF; ?>" method="get">
    <input type="hidden" name="get" value="1" />
    <tr>
      <td width="66%" class="results" valign="top">
        <small><br /></small>

        <div class="query">
          <strong>Find:</strong>&nbsp;&nbsp;<input type=text name="query" size="20"
          value="<?php echo $html_query; ?>" onMouseOver="this.focus()" onFocus="this.select()" />&nbsp;<input
          type="submit" value="Search" default /><br />
          <small><br />

          <input type="checkbox" name="partial"
          <?php if(!empty($partial)) echo " checked"; ?> />&nbsp;Substring&nbsp;&#183;

          <?php if($allow_fulltext): ?>
            <input type="checkbox" name="fulltext"
            <?php if(!empty($fulltext)) echo " checked"; ?> />&nbsp;Full-text&nbsp;&#183;
          <?php endif; ?>

          <?php if($show_case_insensitive): ?>
            <input type="checkbox" name="anycase"
            <?php if(!empty($anycase)) echo " checked"; ?> />&nbsp;Case-insensitive
          <?php endif; ?>
          </small>
        </div>

        <div class="results">
          <br />
          <?php
            // Print search results
            if(!strlen($query_asis)):
              if(strlen($welcome_text)>0):
                //readfile($welcome_text);
                PrintFileUTF($welcome_text);
              else:

          ?>
                <p><big><br /><em>Enter a word or phrase into "Find" field above and
                click Search.</em></big></p>
          <?php
              endif; //strlen($welcome_text)>0)
            elseif(strlen($query)==0):
              echo "<p class=\"error\">An error occured while converting query ",
                "charset. Most likely, phpMyLingvo is set up ",
                "to use GNU Recode, but it's not installed on your system. ",
                "Please install <code>recode</code> or reconfigure phpMyLingvo.</p>";
            else:
              // Initalize search variables
              $cnt=0;
              $substring=$partial;
              if(!$allow_fulltext) $fulltx=false; else $fulltx=$fulltext;
              if(!$show_case_insensitive) $anycase=false;

              // Preventing short substring search
              if(strlen($query)<$min_substring && (!empty($partial) || !empty($fulltext))){
                $substring=false;
                $fulltx=false;
                echo "<p class=\"error\">Query \"$html_query\" is too short for substring or full-text search.</p>";
              };


              echo "<p class=\"res_word\"><big><strong>Search results for: <big class=\"hl0\">$html_query</big>",
                "</strong></big> <small class=\"optinfo\">",
                ((!empty($substring))?"&#183; substring ":""),
                ((!empty($fulltx))?"&#183; fulltext ":""),
                ((!empty($anycase))?"&#183; all cases ":""),
                "</small></p>\n";




              $numart=0;
              for($i=0; $i<sizeof($diclist); $i++){
                $dict=${"use_".$diclist[$i]["Name"]};
                if($dict){
                  $cnt++;
                  if($cnt>$max_dictionaries){
                    echo "<p class=\"error\">You cannot search in more than $max_dictionaries dictionaries at once!</p>\n";
                    $cnt--;
                    break;
                  }
                  if(!empty($fulltx) && $cnt>$max_dictionaries_fulltext){
                    echo "<p class=\"error\">You cannot search in more than $max_dictionaries_fulltext dictionaries at once in full-text mode!</p>\n";
                    $cnt--;
                    break;
                  }
                  $numart+=SearchDict($diclist[$i], $query, $substring, $fulltx, $anycase);
                  if($search_results>=$max_search_results){
                    echo "<p class=\"error\">Max number of search results ($max_search_results) reached.</p>\n";
                    break;
                  }
                }
              }
              if($cnt>0){
                if($numart==0)
                  echo "<p class=\"error\"><em>No matches found.</em></p>\n";
                echo "<p><em>Done (searched through $cnt dictionaries, $numart articles found).</em></p>\n";
                if(!empty($fulltext))
                  echo "<p class=\"note\">Note: full-text search is always case sensitive and allows substrings.</p>\n";
              }else
                echo "<p><big><em>No dictionaries selected.</em></big></p>\n";


            endif;
          ?>
        </div>
      </td>
      <td width="34%" class="dictionaries" valign="top">
        <small><br /></small>
        <center>

<table class="dictgroups">
  <tr><td colspan="3">
    <h2>Dictionaries</h2>
  </td></tr>
  <tr>
    <?php
      echo "<td nowrap valign=\"top\"><p class=\"column\"><small><b><tt>";
      foreach($dicbuttons as $s) echo "$s<br />\n";
      echo "</tt></b></small></p></td>\n<td nowrap valign=top><p class=\"column\"><small><b><tt>";
      foreach($dicbuttonsS as $s) echo "$s<br />\n";
      echo "</tt></b></small></p></td>\n<td nowrap valign=top><p class=\"column\"><small><b><tt>";
      foreach($languagecmds as $s) echo "$s<br />\n";
      echo "</tt></b></small></p></td>\n";
    ?>
  </tr>
  <tr><td colspan="3" class="bordered">
    <table class="dictboxes">
    <?php
      // output dictionary list
      for($i=0; $i<sizeof($diclist); $i++){
        $dict="use_".$diclist[$i]["Name"];
        echo "<tr>",
          "<td><small></small><input type=\"checkbox\" name=\"$dict\"",
              ((!empty(${$dict}))?" checked":""), " />",
          "</small></td><td><small>",
          "<a href=\"dictionaryinfo.php?name=",$diclist[$i]["Name"],"\" ",
          "title=\"Click for information about the dictionary (".$diclist[$i]["Name"].")\" ",
          "target=\"dictinfo\" onClick=\"DictInfoPopup();\">",
          htmlspecialchars($diclist[$i]["Desc"]),
          "</a></small></td>",
          "</tr>\n";
      }
    ?>
    </table>
  </td></tr>
  <tr><td colspan="3" class="bordered cen">
    <p class="column cen" align="center">
    <a href="javascript:SetAll();" title="Click to select all available dictionaries">Select All</a> &#183;
    <a href="javascript:ClearAll();" title="Click to deselect all available dictionaries">Deselect All</a>
    </p>
  </td></tr>
  <tr><td colspan="3"><center>You can use up to <?php
    echo min($max_dictionaries, sizeof($diclist)); ?> dictionaries at once <?php
    if($allow_fulltext): ?><br />(up to <?php
    echo min($max_dictionaries_fulltext, sizeof($diclist)); ?> in full-text search)<?php endif;
  ?></center></td></tr>
</table>


        </center><small><br /></small>
      </td>
    </tr>
    <tr>
      <td colspan="2" class="bottom">
        <center><small><br />
          <a href="http://sageshome.net/oss/phpMyLingvo.php"
          title="phpMyLingvo Homepage">phpMyLingvo</a> version <?php echo $version; ?>,
          &copy; <a href="http://sageshome.net/?id=phpMyLingvo"
          title="Sergey Galin's Homepage">Sergey A. Galin</a>, 2003<br />
          This software is distributed under the terms of the
          <a href="http://www.gnu.org" target="_blank"
          title="GNU/Free Software Foundation Site">GNU</a>
          <a href="http://sageshome.net/GPL.php" target="_blank"
          title="Read GNU General Public License">General Public License</a>
          v2.0 or higher.<br />
          See <a href="readme.html" title="phpMyLingvo Documentation">phpMyLingvo Readme</a> for more information.
          <br />
          <a href="admin.php">Admin Interface&raquo;</a><br /><br />
        </small></center>
      </td>
    </tr>
  </form>
</table>
<?php
  if(strlen($bottom_code)>0)
    readfile($bottom_code);
?>


</body>
</html>

