<?php
  // This software is distributed under GNU General Public License, ver. 2
  // or higher (at your option), released by Free Software Foundation. You can
  // find text of GNU GPL at
  //   http://sageshome.net/GPL.php
  // or
  //   http://www.gnu.org/
  //
  // Copyright(C) Sergey A. Galin, 2003-2004.

  require_once("config.php");
  require_once("charset_functions.php");
  require_once("functions.php");
  require_once("formvars.php");
  OldFormVars("name");
  $name=Get("name");

  if(empty($name)) die("No dictionary specified!");

  LoadDicList();
  $dict=array();
  foreach($diclist as $dic){
    if($dic["Name"]==$name){
      $dict=$dic;
      break;
    }
  }
  if(!sizeof($dict)) die("Dictionary not found!");


  $query="SELECT SUM(1) AS Count FROM $name WHERE art_id>=10";
  $res=$db->query($query);
    if (DB::isError($res)) die ("Error quering article count: ".$res->getMessage());

  $row=$res->fetchrow();
  $art_count=$row["Count"];
  $res->free();

  echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo htmlspecialchars($dict["Desc"]); ?> - View Dictionary Info</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="Description" content="Dictionary information for <?php
    echo htmlspecialchars($dict["Desc"]); ?>" />
  <meta name="Author" content="Sergey A. Galin, http://sageshome.net" />
  <meta name="Keywords" content="<?php
    echo htmlspecialchars($dict["From"]), " ", htmlspecialchars($dict["To"]);
    ?>phpMyLingvo PHPDic PHP Dic MySQL PtkDic GtkDic JaLingvo dictionary glossary GNU GPL FSF" />
  <link rel="StyleSheet" type="text/css" href="style.css" />
</head>
<body class="dictinfo">
<h1><?php echo htmlspecialchars($dict["Desc"]); ?></h1>
<br />
<center>
<table class="grid" width="90%">
  <tr>
    <th>Title:</th>
    <td><?php echo htmlspecialchars($dict["Desc"]); ?>&nbsp;</td>
  </tr>
  <tr>
    <th>Database&nbsp;Name:</th>
    <td><?php echo $dict["Name"]; ?>&nbsp;</td>
  </tr>
  <tr>
    <th>Title&nbsp;Language:</th>
    <td><?php echo $dict["From"]; ?>&nbsp;</td>
  </tr>
  <tr>
    <th>Article&nbsp;Language:</th>
    <td><?php echo $dict["To"]; ?>&nbsp;</td>
  </tr>
  <tr>
    <th>Number&nbsp;Of&nbsp;Articles:</th>
    <td><?php echo $art_count; ?>&nbsp;</td>
  </tr>


</table>
<br />
<form action="<?php echo $PHP_SELF; ?>">
  <input type=button value=" Close Window " onClick="window.close();" />
</form>

</center>

</body>
</html>
