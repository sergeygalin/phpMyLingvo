<?php
  // This software is distributed under GNU General Public License, ver. 2
  // or higher (at your option), released by Free Software Foundation. You can
  // find text of GNU GPL at
  //   http://sageshome.net/GPL.php
  // or
  //   http://www.gnu.org/
  //
  // Copyright(C) Sergey A. Galin, 2003-2004.

  $debug=false;


  require_once("config.php");
  require_once("formvars.php");
  require_once("functions.php");
  CheckAdminLogged();

  //mysql_select_db($database) or
  //  die("Cannot select database $database: ".mysql_error());;
  $dicts_installed=LoadDicList(false);

  OldFormVars("mode", "url", "dictionary", "outfile",
    "dumpupload", "install", "deletefile", "confirmation");

  $mode=Request("mode");
  $url=Get("url");

  if($debug)
    $stage1_url="WORK/SAMPLE1.html";
  else
    $stage1_url="http://sourceforge.net/project/showfiles.php?group_id=78379";

  function LoadURLDots($url, $block=4096){
    flush();
    set_time_limit(120);
    $f=fopen($url, "r") or die("<br>\nFailed to load: ".htmlspecialchars($url)."\n");
    $ret="";
    while(!feof($f)){
      set_time_limit(120);
      $ret.=fread($f, $block);
      echo ". ";
      flush();
    }
    fclose($f);
    echo strlen($ret), " byte(s)";
    return $ret;
  }

  function LoadURLDotsF($url, $out, $block=4096){
    flush();
    set_time_limit(120);
    $f=fopen($url, "r") or die("<br>\nFailed to load: ".htmlspecialchars($url)."\n");
    $outf=fopen($out, "w") or
      die("<br>\nFailed to create: ".htmlspecialchars($out).
        " . Please check directory permissions!\n");
    $size=0;
    while(!feof($f)){
      set_time_limit(120);
      $buf=fread($f, $block);
      $size+=strlen($buf);
      fwrite($outf, $buf);
      echo ". ";
      flush();
    }
    fclose($f);
    fclose($outf);
    echo "$size byte(s)";
  }

  // Find http:// URL's in page
  function FindURLs($page, $base){
    $basearr=parse_url($base);
    $host=$basearr["host"];
    $dir=dirname($base);
    $ret=array();
    while(($pos=strpos(strtolower($page), "href="))!==false){
      if($page{$pos+5}=='"') $page=substr($page, $pos+6);
      else $page=substr($page, $pos+5);
      $end=strpos($page, "\"");
      $end2=strpos($page, ">");
      if($end2!==false){
        if($end===false) $end=$end2;
        else $end=min($end, $end2);
      }
      $url=substr($page, 0, $end);
      if(substr($page, 0, 7)=="http://")
        array_push($ret, $url);
      else{
        if($url{0}=="/") array_push($ret, "http://".$host.$url);
        else array_push($ret, $dir."/".$url);
      }
      $page=substr($page, $end+1);
    }

    //echo "\n\n\n<!--\nBASE: $base\nHOST: $host\n";
    //foreach($ret as $r) echo $r,"\n";
    //echo "-->\n\n\n\n";

    return $ret;
  }

  // Find download links which point to mirror selection in links found
  // on project download page.
  function FindSFDownloadLinks(&$urls){
    $ret=array();
    foreach($urls as $url){
      if(
        strstr($url, "http://prdownloads.sourceforge.net/")!==false &&
        strstr($url, "?download")!==false &&
        strstr($url, ".bz2")!==false &&
        strstr($url, ".tar.")===false
        )
        array_push($ret, $url);
    }
    return $ret;
  }

  // Find download links on a mirror selection page.
  function FindSFDownloadLinks2(&$urls){
    $ret=array();
    foreach($urls as $url){
      if(
        strstr($url, ".bz2?use_mirror=")!==false
        )
        array_push($ret, $url);
    }
    return $ret;
  }

  // Find final download link (when default mirror selected).
  function FindSFDownloadLink3(&$urls){
    foreach($urls as $url){
      if(
        strstr($url, "?use_mirror=")===false &&
        strstr($url, "?download=")===false &&
        strstr($url, ".bz2")!==false &&
        strstr($url, ".tar.")===false
        )
        return $url;
    }
    die("<br />\nDownload URL not found!\n");
  }



  echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Dictionary Installer</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="Author" content="Sergey A. Galin, http://sageshome.net" />
  <link rel="StyleSheet" type="text/css" href="style.css" />
</head>
<body class="admin">
<h1>Dictionary Installer</h1>
<div class="pad10">

<?php
  switch($mode){

    case "download":
      echo "<p class=\"progress\">Loading <a href=\"$stage1_url\" ",
        "target=\"_blank\">$stage1_url</a>...";
      $files_page=LoadURLDots($stage1_url);
      echo "</p>\n<br />\n";
      echo "<h2>Dictionaries Available To Download</h2>\n";
      echo "<br />\n<br />\n<center>\n";
      echo "<form action=\"admin_dictinstall.php\" method=\"get\">\n";
      echo "<input type=\"hidden\" name=\"mode\" value=\"selectmirror\" />\n";
      $urls=FindURLs($files_page, $stage1_url);
      $durls=FindSFDownloadLinks($urls);
      sort($durls);
      echo "Select a dictionary to download:\n";
      echo "<select name=\"url\">\n";
      foreach($durls as $url)
        echo "<option value=\"", htmlspecialchars($url), "\">",
          basename($url, ".bz2?download"), "</option>\n";
      echo "</select>\n";
      echo "<input type=\"submit\" value=\"  Select Mirror  \" />\n";
      echo "</center>\n";
      echo "</form>\n";
      break;

    case "selectmirror":
      if($debug) $url="WORK/SAMPLE2.html";
      echo "<p class=\"progress\">Loading <a href=\"", htmlspecialchars($url), "\" ",
        "target=\"_blank\">", htmlspecialchars($url), "</a>...";
      $files_page=LoadURLDots($url);
      $base=htmlspecialchars(basename($url, ".bz2?download"));
      echo "</p>\n<br />\n";
      echo "<h2>Select a Mirror for $base</h2>\n";
      echo "<br />\n<br />\n<center>\n";
      echo "<form action=\"admin_dictinstall.php\" method=\"get\">\n";
      echo "<input type=\"hidden\" name=\"mode\" value=\"getfile\" />\n";
      $urls=FindURLs($files_page, $url);
      $durls=FindSFDownloadLinks2($urls);
      sort($durls);
      echo "Select a mirror:\n";
      echo "<select name=\"url\">\n";
      foreach($durls as $url){
        $mirror=substr($url, strpos($url, "=")+1);
        echo "<option value=\"", htmlspecialchars($url), "\">",
          htmlspecialchars($mirror), "</option>\n";
      }
      echo "</select>\n";
      echo "<input type=\"submit\" value=\"  Download $base \" />\n";
      echo "</center>\n";
      echo "</form>\n";
      break;

    case "getfile":
      if($debug) $url="WORK/SAMPLE3.html";
      echo "<p class=\"progress\">Loading <a href=\"", htmlspecialchars($url), "\" ",
        "target=\"_blank\">", htmlspecialchars($url), "</a>...";
      $files_page=LoadURLDots($url);
      echo "</p>\n";
      $urls=FindURLs($files_page, $url);
      $url=FindSFDownloadLink3($urls);
      echo "<p>Downloading file from <a href=\"",htmlspecialchars($url), "\" ",
        "target=\"_blank\">", htmlspecialchars($url), "</a>...</p>\n";
      //$base=htmlspecialchars(basename($url, ".bz2?download"));
      $outf="dumps/".basename($url);
      echo "<p>Saving to: ", htmlspecialchars($outf), "</p>\n";
      if(file_exists($outf))
        echo "<p>File already exists, overwriting...</p>\n";
      if(!$debug){
        LoadURLDotsF($url, $outf);
        @chmod($outf, 0666);
      }
      echo "<p><strong><a href=\"admin_dictinstall.php\">Click Here To Continue</a></strong></p>\n";
      break;

    case "deletefile":
      if(strstr($url, "/")!==false || strstr($url, "\\")!==false ||
         substr($url, strlen($url)-4)!==".bz2")
        die("Bad file name!");
      @unlink("dumps/$url");
      echo "<p>File ", htmlspecialchars($url), " deleted.</p>\n";
      echo "<p><strong><a href=\"admin_dictinstall.php\">Click Here To Continue</a></strong></p>\n";
      break;

    /*
    Old `mysqldump`-based dump maker.
    case "makedump":
      $dict=Get("dictionary");
      $to=Get("outfile");
      if(strstr($to, "/")!==false || strstr($to, "\\")!==false ||
         substr($to, strlen($to)-4)!==".bz2" || $to{0}=='.')
        die("<br />\nThe file name must not contain slashes or start from period; extension must be &quot;.bz2&quot;.");
      echo "<p>Dumping $dict to ", htmlspecialchars($to), ", please be patient...</p>\n";
      $fn="dumps/$to";
      flush();
      set_time_limit(180);
      $cmd="mysqldump -h$mysql_host -u$mysql_user".
        ((strlen($mysql_password))?" -p$mysql_password":"").
        " $database $dict | bzip2 -9c > $fn";
      echo "<p>", htmlspecialchars(`$cmd`), "</p>\n";
      @chmod($fn, 0666);
      flush();
      echo "<p>Dumping finished.</p>\n";
      echo "<p><strong><a href=\"admin_dictinstall.php\">Click Here To Continue</a></strong></p>\n";
      break;
    */

    /*
    Old `mysql`-based dump installer.

    case "install":
      $file=Get("url");
      if(strstr($file, "/")!==false || strstr($file, "\\")!==false ||
         substr($file, strlen($file)-4)!==".bz2" || $file{0}=='.')
        die("<br />\nThe file name must not contain slashes or start from period; extension must be &quot;.bz2&quot;.");
      echo "<p>Installing from dump ", htmlspecialchars($file), ", please be patient...</p>\n";
      $fn="dumps/$file";
      flush();
      set_time_limit(180);
      $tmpfile=tempnam("dumps", "~di_tmp");
      $cmd="bzip2 -dc $fn | mysql -h$mysql_host -u$mysql_user ".
        ((strlen($mysql_password))?" -p$mysql_password":"").
        " $database &> $tmpfile";
      //echo $cmd;
      echo "<p>", htmlspecialchars(`$cmd`), "</p>\n";
      echo "<p><strong>";
      readfile($tmpfile);
      echo "</strong></p>\n";
      @unlink($tmpfile);
      flush();
      echo "<p>Installation finished.</p>\n";
      echo "<p><strong><a href=\"admin_dictinstall.php\">Click Here To Continue</a></strong></p>\n";
      break;
      */

    case "delete":
      $dict=Get("dictionary");
      if($dict!==Get("confirmation"))
        die("<br />\nPlease re-enter dictionary DB name in a text box to confirm deletion.\n");
      $res=$db->query("DROP TABLE $dict");
      if(DB::isError($res))
        die("<br />\nSQL error: ".$res->getMessage());
      echo "<p>Dictionary $dict deleted!</p>\n";
      echo "<p><strong><a href=\"admin_dictinstall.php\">Click Here To Continue</a></strong></p>\n";
      break;

    case "upload":
      echo "<p>Receiving upload...</p>\n";
      if(!strlen($HTTP_POST_FILES["putdump"]["tmp_name"]))
        die("<br />\nNo file uploaded!\n");
      $temp=$HTTP_POST_FILES["putdump"]["tmp_name"];
      $name=$HTTP_POST_FILES["putdump"]["name"];
      $size=$HTTP_POST_FILES["putdump"]["size"];
      if(!is_uploaded_file($temp))
        die("<br />\nNot an uploaded file!\n");
      if($name{0}=="." ||
         strpos($name, "/")!==false ||
         strpos($name, "\\")!==false ||
         substr($name, strlen($name)-4)!=".bz2")
        die("<br />\nFile name must not start from period, contains slashes and must end with &quot;.bz2&quot.\n");
      $store="dumps/$name";
      if(file_exists($store)){
        echo "<p>File already exists, overwriting...</p>\n";
        unlink($store);
      }
      move_uploaded_file($temp, $store) or
        die("Failed to move uploaded file to $store. Please ".
            "check that directory has proper permissions (should be ".
            "available for writing by your HTTP server).");
      @chmod($store, 0666);
      echo "<p>File ", htmlspecialchars($name), " uploaded, $size byte(s).</p>\n";
      echo "<p><strong><a href=\"admin_dictinstall.php\">Click Here To Continue</a></strong></p>\n";
      break;

    default:
      ?>
      <br />
      <center>
      <table width="75%">
<?php /*
        <tr>
          <td colspan="5" class="bordered">
            <center>
              <strong>::Information::</strong><br />
              You must have the following utilites
              installed on your computer and located via $PATH
              to have the tool working properly:
              <code>bzip2.</code>
              On Linux/UN*X systems that's usually OK by default,
              but on Window$ it may require some hand work.
              <br />
              <br />
            </center>
          </td>
        </tr>
*/?>
        <tr><th colspan="5">
          <h2>Local Dictionary Dump Directory</h2>
        </th></tr>
        <?php
          $dir=opendir("dumps");
          $count=0;
          while(($file=readdir($dir))!==false){
            //echo $file, "<br>";
            if(substr($file, strlen($file)-4)==".bz2"){
              $hfile=htmlspecialchars($file);
              echo "<tr>\n";
              echo "<td width=\"20%\" class=\"bordered\" nowrap>&nbsp;<a title=\"Click here to download the file to your computer\" href=\"dumps/$hfile\">$hfile</a></td>\n";
              echo "<td width=\"20%\" class=\"bordered\" nowrap><center><small>",
                date("D, Y-m-d h:i:s", filemtime("dumps/$file")), "</small></center></td>\n";

              echo "<td width=\"20%\" class=\"bordered\" align=\"right\" nowrap>",
                filesize("dumps/$file"), " byte(s)</td>\n";
              echo "<td width=\"20%\" class=\"bordered\" nowrap><center><a title=\"Click here to delete the file from dump directory\" onclick=\"javascript:return confirm('Are you sure to delete this file?');\" href=\"admin_dictinstall.php?mode=deletefile&amp;url=$hfile\">Delete Dump</a></center></td>\n";
              echo "<td width=\"20%\" class=\"bordered\" nowrap><center><a title=\"Click here to install the dictionary\" onclick=\"javascript:return confirm('Are you sure to install this dictionary?');\" href=\"admin_installdump.php?file=$hfile\">Install Dictionary</a></center></td>\n";
              echo "</tr>\n";
              $count++;
            }
          }
          closedir($dir);
          if(!$count)
            echo "<tr><td colspan=\"5\"><center><strong>No dictionaries in dump ",
              " directory.</strong></center></td></tr>\n";
        ?>
        <tr><td colspan="5">&nbsp;</td></tr>
        <tr><th colspan="5">
          <h2>Add Dictionary Dumps</h2>
        </th></tr>
        <tr>
          <td colspan="5" class="bordered">
            <center><strong><a href="admin_dictinstall.php?mode=download">Download
              dictionary from phpMyLingvo Sourceforge Page</a></strong></center>
          </td>
        </tr>
        <tr>
          <td colspan="5" class="bordered">
            <br />
            <center>
            <form action="admin_dictinstall.php" method="post"
              enctype="multipart/form-data">
              <input type="hidden" name="mode" value="upload" />
              Upload dictionary dump file via form (.bz2 files only please):
              <input type="file" name="putdump" />
              <input type="submit" value="  Upload  " />
            </form>
            <p>Please note that your webserver doesn't allow form uploads
            larger than <?php echo get_cfg_var("upload_max_filesize"); ?>
            so this method will fail for bigger
            dictionary. You may also upload your dumps right into
            <code>your phpMyLingvo installation directory/dumps</code>
            via <strong>FTP</strong>.
            Make sure to set write permission on the files to allow
            writing by your Web server (<kbd>chmod 0666</kbd> should
            work) in this case.</p>
            </center>
          </td>
        </tr>


        <?php if($dicts_installed): ?>

        <tr><td colspan="5">&nbsp;</td></tr>
        <tr><th colspan="5">
          <h2>Dump Dictionary From Database</h2>
        </th></tr>
        <tr>
          <td colspan="5" class="bordered">
            <br />
            <center>
            <form action="admin_dumpdictionary.php" method="get" name="dumpform">
              Select a dictionary:
              <select name="dictionary" onchange="javascript:document.dumpform.file.value=document.dumpform.dictionary.value+'.bz2';">
              <?php
                foreach($diclist as $dict){
                  // Name Desc From To
                  echo "<option value=\"$dict[Name]\">",
                    htmlspecialchars($dict["Name"]." - ".$dict["Desc"]),
                    "</option>\n";
                }
              ?>
              </select>
              <br />
              Save To:
              <input type="text" size="20" name="file" />
              <input type="submit" value="  Dump  "
              onclick="javascript:return confirm('Are you sure to dump this dictionary?');" />
            </form>
            <script type="text/javascript"><!--
              document.dumpform.file.value=
                document.dumpform.dictionary.value+'.bz2';
              //-td->
            </script>
            </center>
          </td>
        </tr>




        <tr><td colspan="5">&nbsp;</td></tr>
        <tr><th colspan="5">
          <h2>Delete Dictionary From Database</h2>
        </th></tr>
        <tr>
          <td colspan="5" class="bordered">
            <br />
            <center>
            <form action="admin_dictinstall.php" method="get" name="delform">
              <input type="hidden" name="mode" value="delete" />
              Select a dictionary:
              <select name="dictionary">
              <?php
                foreach($diclist as $dict){
                  // Name Desc From To
                  echo "<option value=\"$dict[Name]\">",
                    htmlspecialchars($dict["Name"]." - ".$dict["Desc"]),
                    "</option>\n";
                }
              ?>
              </select>
              <br />
              Re-type dictionary DB name for confirmation:
              <input type="text" size="15" name="confirmation" />
              <input type="submit" value="  Delete  "
              onclick="javascript:if(document.delform.dictionary.value==document.delform.confirmation.value) return confirm('Are you sure to delete this dictionary?'); else{ alert('Please re-type dictionary name!'); document.delform.confirmation.focus(); return false; };" />
            </form>
            </center>
          </td>
        </tr>

        <?php endif; /* if($dicts_installed) */ ?>



      </table>
      </center>



      <?php
  }
?>

<br />
<center><a href="admin.php">Return to Admin</a></center>

</div>
</body>
</html>
