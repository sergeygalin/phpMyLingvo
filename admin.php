<?php
  // This software is distributed under GNU General Public License, ver. 2
  // or higher (at your option), released by Free Software Foundation. You can
  // find text of GNU GPL at
  //   http://sageshome.net/GPL.php
  // or
  //   http://www.gnu.org/
  //
  // Copyright(C) Sergey A. Galin, 2003-2004.

  $version_file=fopen("version.txt", "r");
  $version=trim(fgets($version_file, 100));
  fclose($version_file);

  require_once("config.php");
  require_once("functions.php");
  require_once("formvars.php");

  OldFormVars("logout", "password");

  if(Request("admin_password")!==false)
    die("Don't even try to pass admin_password via HTTP!\n");
  if(!isset($admin_password))
    die("\$admin_password is not set, Admin disabled!\n");
  if(!strlen($admin_password))
    die("\$admin_password is an empty string, Admin disabled!\n");

/*
  if(!isset($mysql_host) || !isset($mysql_user) || !isset($mysql_password))
    die("<br>\nYou are using an old configuration file and \$mysql_host, \$mysql_user and \$mysql_password variables needed by Dictionary Installer are not set.\n");
*/

  $message="";

  if(Get("logout")){
    $_SESSION["phpmylingvo_admin"]=false;
    $message.="Logged out.";
  }elseif(($password=Post("password"))!==false){
    if($password==$admin_password){
      $_SESSION["phpmylingvo_admin"]=1;
//      die("OK?");
//      echo "OK"; exit;
      Redirect("admin.php");
    }else{
      $_SESSION["phpmylingvo_admin"]=false;
      $message.="Wrong password!";
    }
  }

  echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo $title; ?> Admin</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="Author" content="Sergey A. Galin, http://sageshome.net" />
  <link rel="StyleSheet" type="text/css" href="style.css" />
</head>
<body class="admin">
<h1><?php echo $title; ?> Admin</h1>
<div class="pad10">

<br />
<center><strong>phpMyLingvo Admin is under construction.
Links to unimplemented features are disabled.</strong></center>
<br />

<?php
  if(strlen($message))
    echo "<center><strong>$message</strong></center>\n";
?>

<center>

<?php if($_SESSION["phpmylingvo_admin"]): ?>
<table class="menu" width="400">
  <tr>
    <td>
      <h2>Admin Functions</h2>
    </td>
  </tr>
  <tr>
    <td class="bordered">
      <p class="column">
        Go to:<br />
        <a href="index.php">phpMyLingvo</a><br />
        <a href="readme.html">Readme</a><br />
        <a href="history.html">History</a><br />
      </p>
    </td>
  </tr>

  <tr><td>
    <p class="column">
      <a href="admin_dictinstall.php">Dictionary Installer</a><br />
      <!--a href="admin_dumpdictionary.php">Dictionary Dump</a><br /-->
    </p>
  </td></tr>
  <tr><td class="bordered">
    <p class="column"><a href="admin.php?logout=1"
    title="Finish working with Admin and lock it with a password.">Log Out</a></p>
  </td></tr>
</table>

<?php else: // Not logged in! ?>

<form action="admin.php" method="post">
<table class="menu">
  <tr>
    <td colspan="2">
      <h2>Login</h2>
    </td>
  </tr>
  <tr>
    <th class="pad3">Password:</th>
    <td class="pad3">
      <input type="password" name="password" size="20" />
      <input type="submit" value="  Login  " default />
    </td>
  </tr>
</table>
</form>
<?php endif; ?>

</center>

</div>
</body>
</html>